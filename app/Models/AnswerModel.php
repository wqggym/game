<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerModel extends Model
{
    protected $guarded = [];
    protected $table = 'answer';
    public function topic_name(){
        return $this->hasOne(TopicModel::class,'id','tid');
    }
}
