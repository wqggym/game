<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopicModel extends Model
{
    protected $guarded = [];
    protected $table = 'topic';

    public $timestamps = false;

    public function template(){
        return $this->belongsTo(AnswerModel::class);
    }
}
