<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordModel extends Model
{
    protected $guarded = [];
    protected $table = 'record';
}
