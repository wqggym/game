<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModels extends Model
{
    protected $table = 'vehicle';

    public $timestamps = false;

}
