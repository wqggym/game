<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointModel extends Model
{
    protected $table = 'appoint';
    public function vehicle_name(){
        return $this->hasOne(VehicleModels::class,'id','vid');
    }
}
