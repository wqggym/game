<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('topic', 'TopicController');
    $router->resource('answer', 'AnswerController');
    $router->resource('appoint', 'AppointController');
    $router->resource('record', 'RecordController');
    $router->resource('vehicle', 'VehicleController');

});
