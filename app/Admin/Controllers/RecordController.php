<?php

namespace App\Admin\Controllers;

use App\Models\RecordModel;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class RecordController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('参与游戏测试列表')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('参与游戏测试列表')
            ->description('查看')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('参与游戏测试列表')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('参与游戏测试列表')
            ->description('录入')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RecordModel);

        $grid->id('ID')->sortable();
        $grid->model()->orderBy('ID', 'desc');
        $grid->specify('最终选择');
        $grid->name('车型');
        $grid->user('参与用户姓名');
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');
        $grid->filter(function ($filter) {
            $filter->like('specify','最终选择');
            $filter->like('cars','车型');
            $filter->like('user','参与用户姓名');
            $filter->between('created_at','创建时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
            $filter->between('updated_at','更新时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RecordModel::findOrFail($id));

        $show->id('ID');
        $show->specify('最终选择');
        $show->name('车型');
        $show->user('参与用户姓名');
        $show->created_at('创建时间');
        $show->updated_at('更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RecordModel);

        $form->display('ID');
        $form->display('name','车型');
        $form->display('specify','最终选择')->help('用于选择时，设置列如：半角逗号分开如A,A,A,A或1,1,1,1');
        $form->display('user','参与用户姓名');
        $form->display('created_at', '创建时间');
        $form->display('updated_at', '更新时间');

        return $form;
    }
}
