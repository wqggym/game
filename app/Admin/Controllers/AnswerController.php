<?php

namespace App\Admin\Controllers;

use App\Models\AnswerModel;
use App\Http\Controllers\Controller;
use App\Models\TopicModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AnswerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('选择答案')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('选择答案')
            ->description('查看')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('选择答案')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('选择答案')
            ->description('录入')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AnswerModel);

        $grid->id('ID')->sortable();
        $grid->model()->orderBy('tid', 'asc');
        $grid->tid('题id')->sortable();
        $grid->topic_name()->name('所属题');
        $grid->name('标题');
        $grid->sort('排序')->sortable();
        $grid->specify('下标');
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');
        $grid->filter(function ($filter) {
            $filter->equal('tid','所属题')->select(
                TopicModel::all()->pluck('name','id')
            );
            $filter->like('name','标题')->placeholder('请输入标题');
            $filter->between('created_at','创建时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
            $filter->between('updated_at','更新时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AnswerModel::findOrFail($id));

        $show->id('ID');
        $show->topic_name('所属题')->as(function () {
            return $this->topic_name->name;
        });
        $show->name('标题');
        $show->sort('排序');
        $show->specify('下标');
        $show->created_at('创建时间');
        $show->updated_at('更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AnswerModel);

        $form->display('ID');
        $form->select('tid', '所属题')->options(
            TopicModel::all()->pluck('name','id')
        );
        $form->text('name','标题');
        $form->number('sort','排序');
        $form->text('specify','下标')->help('用于选择时，设置列如：A、B、C、D或1、2、3、4');
        $form->display('created_at', '创建时间');
        $form->display('updated_at', '更新时间');

        return $form;
    }
}
