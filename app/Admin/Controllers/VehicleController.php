<?php

namespace App\Admin\Controllers;

use App\Models\VehicleModels;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class VehicleController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('车型')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('车型')
            ->description('查看')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('车型')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('车型')
            ->description('录入')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new VehicleModels);

        $grid->id('ID');
        $grid->name('车型名称');
        $grid->imgurl('车型图片')->image('',100,100);
        $grid->filter(function ($filter) {
            $filter->like('name','车型名称')->placeholder('请输入车型名称');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(VehicleModels::findOrFail($id));

        $show->id('ID');
        $show->name('车型名称');
        $show->imgurl('车型图片')->image('',100,100);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new VehicleModels);

        $form->display('ID');
        $form->text('name','车型名称');
        $form->image('imgurl','车型图片')->uniqueName()->removable() ->help('xxx*xxx像素');
//        ->resize(1920,420)
        return $form;
    }
}
