<?php

namespace App\Admin\Controllers;

use App\Models\AppointModel;
use App\Http\Controllers\Controller;
use App\Models\VehicleModels;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AppointController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('指定最终得出车型')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('指定最终得出车型')
            ->description('查看')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('指定最终得出车型')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('指定最终得出车型')
            ->description('录入')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AppointModel);

        $grid->id('ID')->sortable();
        $grid->model()->orderBy('ID', 'desc');
        $grid->specify('最终选择');
        $grid->cars('测试结果是什么车');
        $grid->vehicle_name()->name('所属车型');
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');
        $grid->filter(function ($filter) {
            $filter->like('specify','最终选择');
            $filter->like('cars','测试结果是什么车');
            $filter->equal('vid','所属车型')->select(
                VehicleModels::all()->pluck('name','id')
            );
            $filter->between('created_at','创建时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
            $filter->between('updated_at','更新时间')->datetime(['format'=>'YYYY-MM-DD HH:mm:ss']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AppointModel::findOrFail($id));

        $show->id('ID');
        $show->specify('最终选择');
        $show->cars('测试结果是什么车');
        $show->vehicle_name('所属车型')->as(function () {
            return $this->vehicle_name->name;
        });
        $show->created_at('创建时间');
        $show->updated_at('更新时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AppointModel);

        $form->display('ID');
        $form->text('specify','最终选择')->help('用于选择时，设置列如：半角逗号分开如A,A,A,A或1,1,1,1');
        $form->text('cars','测试结果是什么车');
        $form->select('vid', '所属车型')->options(
            VehicleModels::all()->pluck('name','id')
        );
        $form->display('created_at', '创建时间');
        $form->display('updated_at', '更新时间');

        return $form;
    }
}
