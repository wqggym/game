<?php

namespace App\Http\Controllers\Api;

use App\Models\RecordModel;
use App\Models\VehicleModels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecordController extends Controller
{
    public function index(Request $request)
    {
        $sex = $request->sex;
        $single = $request->single;
        $most = $request->most;
        $like = $request->like;
        $specify = $sex.','.$single.','.$most.','.$like;
        $appointObj = new AppointController();
        $request->offsetSet('specify',$specify);
        $iRs = $appointObj->index($request);
        if($iRs['code'] == 500){
            return $this->error();
        }
        $rs = json_decode(json_encode($iRs['data']),true);
        $vehicleRs = VehicleModels::where('id',$rs['vid'])->get();
        $name = $rs['cars'];
        if(empty($request->name)){
            return $this->error('user','用户名不能为空！');
        }
            $rsRe = RecordModel::create([
                'name' => $name,
                'specify' => $specify,
                'user' => $request->name,
            ]);
        if(!empty($rsRe->id)){
            $arr = [
                'name'=>$rsRe->user,
                'car'=>$name,
                'model'=>env('APP_URL','').'/uploads/'.$vehicleRs[0]['imgurl']
            ];
            return [
                'code'=>200,
                'data'=>$arr,
                'message'=>'成功！'
            ];
        }else{
            return $this->error($rs);
        }
    }
    protected function error($arr = '',$ny = '请传正确的参数或值')
    {
        return [
            'code'=>500,
            'data'=>$arr,
            'message'=>$ny
        ];
    }
}
