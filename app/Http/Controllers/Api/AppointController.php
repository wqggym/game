<?php

namespace App\Http\Controllers\Api;

use App\Models\AppointModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointController extends Controller
{
    public function index(Request $request)
    {
        if(empty($request->specify)){
            return $this->error('specify','最终选择不能为空！');
        }
        $rs = AppointModel::where('specify',$request->specify)->get();
        if(!empty($rs[0]->id)){
            return [
                'code'=>200,
                'data'=>$rs[0],
                'message'=>'成功！'
            ];
        }else{
            return $this->error($rs);
        }
    }
    protected function error($arr = '',$ny = '请传正确的参数或值')
    {
        return [
            'code'=>500,
            'data'=>$arr,
            'message'=>$ny
        ];
    }
}
