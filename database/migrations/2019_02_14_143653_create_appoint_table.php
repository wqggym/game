<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appoint', function (Blueprint $table) {
            $table->increments('id');
            $table->string('specify')->comment('最终选择半角逗号分开如A,A,A,A')->nullable();
            $table->string('cars')->comment('测试结果是什么车')->nullable();
            $table->mediumInteger('vid')->comment('所属车型')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appoint');
    }
}
