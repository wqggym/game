/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306_root_root
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : game

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-02-18 16:16:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '0', '1', '环境信息', 'fa-bar-chart', '/', null, null, '2019-02-14 14:12:29');
INSERT INTO `admin_menu` VALUES ('2', '0', '2', '后台配置', 'fa-tasks', null, null, null, '2019-02-14 14:12:40');
INSERT INTO `admin_menu` VALUES ('3', '2', '3', '管理员', 'fa-users', 'auth/users', null, null, '2019-02-14 14:12:50');
INSERT INTO `admin_menu` VALUES ('4', '2', '4', '角色', 'fa-user', 'auth/roles', null, null, '2019-02-14 14:13:04');
INSERT INTO `admin_menu` VALUES ('5', '2', '5', '权限', 'fa-ban', 'auth/permissions', null, null, '2019-02-14 14:13:14');
INSERT INTO `admin_menu` VALUES ('6', '2', '6', '菜单', 'fa-bars', 'auth/menu', null, null, '2019-02-14 14:13:26');
INSERT INTO `admin_menu` VALUES ('7', '2', '7', '操作日志', 'fa-history', 'auth/logs', null, null, '2019-02-14 14:13:36');
INSERT INTO `admin_menu` VALUES ('8', '0', '8', 'Helpers', 'fa-gears', '', null, '2019-02-14 03:31:41', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('9', '8', '9', 'Scaffold', 'fa-keyboard-o', 'helpers/scaffold', null, '2019-02-14 03:31:41', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('10', '8', '10', 'Database terminal', 'fa-database', 'helpers/terminal/database', null, '2019-02-14 03:31:41', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('11', '8', '11', 'Laravel artisan', 'fa-terminal', 'helpers/terminal/artisan', null, '2019-02-14 03:31:41', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('12', '8', '12', 'Routes', 'fa-list-alt', 'helpers/routes', null, '2019-02-14 03:31:41', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('13', '0', '13', '站点管理', 'fa-asterisk', null, null, '2019-02-14 15:06:22', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('14', '13', '14', '题', 'fa-bars', 'topic', null, '2019-02-14 15:07:20', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('15', '13', '15', '选择答案', 'fa-bars', 'answer', null, '2019-02-14 15:08:18', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('16', '13', '16', '指定最终得出车型', 'fa-bars', 'appoint', null, '2019-02-14 15:09:15', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('17', '13', '18', '参与游戏测试列表', 'fa-bars', 'record', null, '2019-02-14 15:09:48', '2019-02-18 14:58:40');
INSERT INTO `admin_menu` VALUES ('18', '13', '17', '车型', 'fa-bars', 'vehicle', null, '2019-02-18 14:18:06', '2019-02-18 14:58:40');

-- ----------------------------
-- Table structure for `admin_operation_log`
-- ----------------------------
DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=597 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_operation_log
-- ----------------------------
INSERT INTO `admin_operation_log` VALUES ('1', '1', 'admin', 'GET', '127.0.0.1', '[]', '2019-02-14 03:29:39', '2019-02-14 03:29:39');
INSERT INTO `admin_operation_log` VALUES ('2', '1', 'admin', 'GET', '127.0.0.1', '[]', '2019-02-14 14:11:06', '2019-02-14 14:11:06');
INSERT INTO `admin_operation_log` VALUES ('3', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:11:16', '2019-02-14 14:11:16');
INSERT INTO `admin_operation_log` VALUES ('4', '1', 'admin/auth/menu/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:12:18', '2019-02-14 14:12:18');
INSERT INTO `admin_operation_log` VALUES ('5', '1', 'admin/auth/menu/1', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u73af\\u5883\\u4fe1\\u606f\",\"icon\":\"fa-bar-chart\",\"uri\":\"\\/\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:12:29', '2019-02-14 14:12:29');
INSERT INTO `admin_operation_log` VALUES ('6', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:12:29', '2019-02-14 14:12:29');
INSERT INTO `admin_operation_log` VALUES ('7', '1', 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:12:33', '2019-02-14 14:12:33');
INSERT INTO `admin_operation_log` VALUES ('8', '1', 'admin/auth/menu/2', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u540e\\u53f0\\u914d\\u7f6e\",\"icon\":\"fa-tasks\",\"uri\":null,\"roles\":[\"1\",null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:12:40', '2019-02-14 14:12:40');
INSERT INTO `admin_operation_log` VALUES ('9', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:12:40', '2019-02-14 14:12:40');
INSERT INTO `admin_operation_log` VALUES ('10', '1', 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:12:43', '2019-02-14 14:12:43');
INSERT INTO `admin_operation_log` VALUES ('11', '1', 'admin/auth/menu/3', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u7ba1\\u7406\\u5458\",\"icon\":\"fa-users\",\"uri\":\"auth\\/users\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:12:50', '2019-02-14 14:12:50');
INSERT INTO `admin_operation_log` VALUES ('12', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:12:50', '2019-02-14 14:12:50');
INSERT INTO `admin_operation_log` VALUES ('13', '1', 'admin/auth/menu/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:12:52', '2019-02-14 14:12:52');
INSERT INTO `admin_operation_log` VALUES ('14', '1', 'admin/auth/menu/4', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u89d2\\u8272\",\"icon\":\"fa-user\",\"uri\":\"auth\\/roles\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:13:03', '2019-02-14 14:13:03');
INSERT INTO `admin_operation_log` VALUES ('15', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:13:04', '2019-02-14 14:13:04');
INSERT INTO `admin_operation_log` VALUES ('16', '1', 'admin/auth/menu/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:13:06', '2019-02-14 14:13:06');
INSERT INTO `admin_operation_log` VALUES ('17', '1', 'admin/auth/menu/5', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u6743\\u9650\",\"icon\":\"fa-ban\",\"uri\":\"auth\\/permissions\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:13:13', '2019-02-14 14:13:13');
INSERT INTO `admin_operation_log` VALUES ('18', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:13:14', '2019-02-14 14:13:14');
INSERT INTO `admin_operation_log` VALUES ('19', '1', 'admin/auth/menu/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:13:17', '2019-02-14 14:13:17');
INSERT INTO `admin_operation_log` VALUES ('20', '1', 'admin/auth/menu/6', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u83dc\\u5355\",\"icon\":\"fa-bars\",\"uri\":\"auth\\/menu\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:13:26', '2019-02-14 14:13:26');
INSERT INTO `admin_operation_log` VALUES ('21', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:13:26', '2019-02-14 14:13:26');
INSERT INTO `admin_operation_log` VALUES ('22', '1', 'admin/auth/menu/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:13:29', '2019-02-14 14:13:29');
INSERT INTO `admin_operation_log` VALUES ('23', '1', 'admin/auth/menu/7', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u64cd\\u4f5c\\u65e5\\u5fd7\",\"icon\":\"fa-history\",\"uri\":\"auth\\/logs\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 14:13:36', '2019-02-14 14:13:36');
INSERT INTO `admin_operation_log` VALUES ('24', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:13:36', '2019-02-14 14:13:36');
INSERT INTO `admin_operation_log` VALUES ('25', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 14:13:44', '2019-02-14 14:13:44');
INSERT INTO `admin_operation_log` VALUES ('26', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 14:16:46', '2019-02-14 14:16:46');
INSERT INTO `admin_operation_log` VALUES ('27', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"topic\",\"model_name\":\"App\\\\Models\\\\TopicModel\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\TopicController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6807\\u9898\"},{\"name\":\"sort\",\"type\":\"bigInteger\",\"nullable\":\"on\",\"key\":null,\"default\":\"0\",\"comment\":\"\\u6392\\u5e8f\"}],\"primary_key\":\"id\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 14:28:39', '2019-02-14 14:28:39');
INSERT INTO `admin_operation_log` VALUES ('28', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2019-02-14 14:28:40', '2019-02-14 14:28:40');
INSERT INTO `admin_operation_log` VALUES ('29', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"answer\",\"model_name\":\"App\\\\Models\\\\AnswerModel\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\AnswerController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":\"\\u6807\\u9898\"},{\"name\":\"sort\",\"type\":\"bigInteger\",\"nullable\":\"on\",\"key\":null,\"default\":\"0\",\"comment\":\"\\u6392\\u5e8f\"},{\"name\":\"specify\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u4e0b\\u6807(\\u7528\\u4e8e\\u9009\\u62e9\\u65f6)\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 14:33:20', '2019-02-14 14:33:20');
INSERT INTO `admin_operation_log` VALUES ('30', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2019-02-14 14:33:20', '2019-02-14 14:33:20');
INSERT INTO `admin_operation_log` VALUES ('31', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"appoint\",\"model_name\":\"App\\\\Models\\\\AppointModel\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\AppointController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"specify\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6700\\u7ec8\\u9009\\u62e9\\u534a\\u89d2\\u9017\\u53f7\\u5206\\u5f00\\u5982A,A,A,A\"},{\"name\":\"cars\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u6d4b\\u8bd5\\u7ed3\\u679c\\u662f\\u4ec0\\u4e48\\u8f66\"}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 14:36:53', '2019-02-14 14:36:53');
INSERT INTO `admin_operation_log` VALUES ('32', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2019-02-14 14:36:54', '2019-02-14 14:36:54');
INSERT INTO `admin_operation_log` VALUES ('33', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"record\",\"model_name\":\"App\\\\Models\\\\RecordModel\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\RecordController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"specify\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 14:39:52', '2019-02-14 14:39:52');
INSERT INTO `admin_operation_log` VALUES ('34', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2019-02-14 14:39:52', '2019-02-14 14:39:52');
INSERT INTO `admin_operation_log` VALUES ('35', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:06:05', '2019-02-14 15:06:05');
INSERT INTO `admin_operation_log` VALUES ('36', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":null,\"icon\":\"fa-bars\",\"uri\":\"\\u7ad9\\u70b9\\u7ba1\\u7406\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:06:16', '2019-02-14 15:06:16');
INSERT INTO `admin_operation_log` VALUES ('37', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:06:16', '2019-02-14 15:06:16');
INSERT INTO `admin_operation_log` VALUES ('38', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u7ad9\\u70b9\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:06:22', '2019-02-14 15:06:22');
INSERT INTO `admin_operation_log` VALUES ('39', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:06:22', '2019-02-14 15:06:22');
INSERT INTO `admin_operation_log` VALUES ('40', '1', 'admin/auth/menu/13/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:06:31', '2019-02-14 15:06:31');
INSERT INTO `admin_operation_log` VALUES ('41', '1', 'admin/auth/menu/13', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u7ad9\\u70b9\\u7ba1\\u7406\",\"icon\":\"fa-asterisk\",\"uri\":null,\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 15:06:38', '2019-02-14 15:06:38');
INSERT INTO `admin_operation_log` VALUES ('42', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:06:39', '2019-02-14 15:06:39');
INSERT INTO `admin_operation_log` VALUES ('43', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u9898\",\"icon\":\"fa-bars\",\"uri\":\"topic\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:07:20', '2019-02-14 15:07:20');
INSERT INTO `admin_operation_log` VALUES ('44', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:07:21', '2019-02-14 15:07:21');
INSERT INTO `admin_operation_log` VALUES ('45', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u9009\\u62e9\\u7b54\\u6848\",\"icon\":\"fa-bars\",\"uri\":\"answer\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:08:18', '2019-02-14 15:08:18');
INSERT INTO `admin_operation_log` VALUES ('46', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:08:18', '2019-02-14 15:08:18');
INSERT INTO `admin_operation_log` VALUES ('47', '1', 'admin/auth/menu/15/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:08:22', '2019-02-14 15:08:22');
INSERT INTO `admin_operation_log` VALUES ('48', '1', 'admin/auth/menu/15', 'PUT', '127.0.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u9009\\u62e9\\u7b54\\u6848\",\"icon\":\"fa-bars\",\"uri\":\"answer\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/auth\\/menu\"}', '2019-02-14 15:08:28', '2019-02-14 15:08:28');
INSERT INTO `admin_operation_log` VALUES ('49', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:08:28', '2019-02-14 15:08:28');
INSERT INTO `admin_operation_log` VALUES ('50', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u6307\\u5b9a\\u6700\\u7ec8\\u5f97\\u51fa\\u8f66\\u578b\",\"icon\":\"fa-bars\",\"uri\":\"appoint\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:09:15', '2019-02-14 15:09:15');
INSERT INTO `admin_operation_log` VALUES ('51', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:09:15', '2019-02-14 15:09:15');
INSERT INTO `admin_operation_log` VALUES ('52', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u53c2\\u4e0e\\u6e38\\u620f\\u6d4b\\u8bd5\\u5217\\u8868\",\"icon\":\"fa-bars\",\"uri\":\"record\",\"roles\":[null],\"permission\":null,\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:09:47', '2019-02-14 15:09:47');
INSERT INTO `admin_operation_log` VALUES ('53', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:09:48', '2019-02-14 15:09:48');
INSERT INTO `admin_operation_log` VALUES ('54', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-14 15:09:57', '2019-02-14 15:09:57');
INSERT INTO `admin_operation_log` VALUES ('55', '1', 'admin/topic', 'GET', '127.0.0.1', '[]', '2019-02-14 15:24:20', '2019-02-14 15:24:20');
INSERT INTO `admin_operation_log` VALUES ('56', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:25:57', '2019-02-14 15:25:57');
INSERT INTO `admin_operation_log` VALUES ('57', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:26:00', '2019-02-14 15:26:00');
INSERT INTO `admin_operation_log` VALUES ('58', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:26:02', '2019-02-14 15:26:02');
INSERT INTO `admin_operation_log` VALUES ('59', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:33:59', '2019-02-14 15:33:59');
INSERT INTO `admin_operation_log` VALUES ('60', '1', 'admin/record', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:34:01', '2019-02-14 15:34:01');
INSERT INTO `admin_operation_log` VALUES ('61', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:34:05', '2019-02-14 15:34:05');
INSERT INTO `admin_operation_log` VALUES ('62', '1', 'admin/topic/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:34:20', '2019-02-14 15:34:20');
INSERT INTO `admin_operation_log` VALUES ('63', '1', 'admin/topic', 'POST', '127.0.0.1', '{\"name\":\"\\u60a8\\u662f\\uff1f\",\"sort\":\"0\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/topic\"}', '2019-02-14 15:34:34', '2019-02-14 15:34:34');
INSERT INTO `admin_operation_log` VALUES ('64', '1', 'admin/topic/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:34:35', '2019-02-14 15:34:35');
INSERT INTO `admin_operation_log` VALUES ('65', '1', 'admin/topic', 'POST', '127.0.0.1', '{\"name\":\"\\u662f\\u5426\\u5355\\u8eab\\uff1f\",\"sort\":\"1\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:35:03', '2019-02-14 15:35:03');
INSERT INTO `admin_operation_log` VALUES ('66', '1', 'admin/topic', 'GET', '127.0.0.1', '[]', '2019-02-14 15:35:03', '2019-02-14 15:35:03');
INSERT INTO `admin_operation_log` VALUES ('67', '1', 'admin/topic/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:35:05', '2019-02-14 15:35:05');
INSERT INTO `admin_operation_log` VALUES ('68', '1', 'admin/topic', 'POST', '127.0.0.1', '{\"name\":\"\\u9009\\u8f66\\u65f6\\uff0c\\u60a8\\u6700\\u5728\\u610f\\u7684\\u4e00\\u70b9\\u662f\",\"sort\":\"2\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/topic\"}', '2019-02-14 15:35:43', '2019-02-14 15:35:43');
INSERT INTO `admin_operation_log` VALUES ('69', '1', 'admin/topic/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:35:44', '2019-02-14 15:35:44');
INSERT INTO `admin_operation_log` VALUES ('70', '1', 'admin/topic', 'POST', '127.0.0.1', '{\"name\":\"\\u60a8\\u559c\\u6b22\\u7684\\u751f\\u6d3b\\u65b9\\u5f0f\\u662f\\uff1f\",\"sort\":\"3\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:36:09', '2019-02-14 15:36:09');
INSERT INTO `admin_operation_log` VALUES ('71', '1', 'admin/topic/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:36:09', '2019-02-14 15:36:09');
INSERT INTO `admin_operation_log` VALUES ('72', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:37:15', '2019-02-14 15:37:15');
INSERT INTO `admin_operation_log` VALUES ('73', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"sort\",\"type\":\"desc\"}}', '2019-02-14 15:37:35', '2019-02-14 15:37:35');
INSERT INTO `admin_operation_log` VALUES ('74', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"sort\",\"type\":\"asc\"}}', '2019-02-14 15:37:36', '2019-02-14 15:37:36');
INSERT INTO `admin_operation_log` VALUES ('75', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:48:43', '2019-02-14 15:48:43');
INSERT INTO `admin_operation_log` VALUES ('76', '1', 'admin/answer/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:48:46', '2019-02-14 15:48:46');
INSERT INTO `admin_operation_log` VALUES ('77', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"1\",\"name\":\"\\u5c0f\\u54e5\\u54e5\",\"sort\":\"0\",\"specify\":\"A\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/answer\"}', '2019-02-14 15:49:32', '2019-02-14 15:49:32');
INSERT INTO `admin_operation_log` VALUES ('78', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:49:32', '2019-02-14 15:49:32');
INSERT INTO `admin_operation_log` VALUES ('79', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"1\",\"name\":\"\\u5c0f\\u54e5\\u54e5\",\"sort\":\"0\",\"specify\":\"A\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:54:12', '2019-02-14 15:54:12');
INSERT INTO `admin_operation_log` VALUES ('80', '1', 'admin/answer', 'GET', '127.0.0.1', '[]', '2019-02-14 15:54:12', '2019-02-14 15:54:12');
INSERT INTO `admin_operation_log` VALUES ('81', '1', 'admin/answer/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:54:17', '2019-02-14 15:54:17');
INSERT INTO `admin_operation_log` VALUES ('82', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"1\",\"name\":\"\\u5c0f\\u59d0\\u59d0\",\"sort\":\"1\",\"specify\":\"B\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/answer\"}', '2019-02-14 15:54:38', '2019-02-14 15:54:38');
INSERT INTO `admin_operation_log` VALUES ('83', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:54:38', '2019-02-14 15:54:38');
INSERT INTO `admin_operation_log` VALUES ('84', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"2\",\"name\":\"\\u662f\",\"sort\":\"0\",\"specify\":\"A\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:55:01', '2019-02-14 15:55:01');
INSERT INTO `admin_operation_log` VALUES ('85', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:55:01', '2019-02-14 15:55:01');
INSERT INTO `admin_operation_log` VALUES ('86', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"2\",\"name\":\"\\u5426\",\"sort\":\"1\",\"specify\":\"B\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:55:12', '2019-02-14 15:55:12');
INSERT INTO `admin_operation_log` VALUES ('87', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:55:12', '2019-02-14 15:55:12');
INSERT INTO `admin_operation_log` VALUES ('88', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"3\",\"name\":\"\\u989c\\u503c\\u7a81\\u51fa\",\"sort\":\"0\",\"specify\":\"A\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:56:09', '2019-02-14 15:56:09');
INSERT INTO `admin_operation_log` VALUES ('89', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:56:09', '2019-02-14 15:56:09');
INSERT INTO `admin_operation_log` VALUES ('90', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"3\",\"name\":\"\\u8282\\u80fd\\u73af\\u4fdd\",\"sort\":\"1\",\"specify\":\"B\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:56:24', '2019-02-14 15:56:24');
INSERT INTO `admin_operation_log` VALUES ('91', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:56:24', '2019-02-14 15:56:24');
INSERT INTO `admin_operation_log` VALUES ('92', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"3\",\"name\":\"\\u7a7a\\u95f4\\u5927\",\"sort\":\"2\",\"specify\":\"C\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:56:40', '2019-02-14 15:56:40');
INSERT INTO `admin_operation_log` VALUES ('93', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:56:40', '2019-02-14 15:56:40');
INSERT INTO `admin_operation_log` VALUES ('94', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"3\",\"name\":\"\\u914d\\u7f6e\\u9ad8\",\"sort\":\"3\",\"specify\":\"D\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:56:56', '2019-02-14 15:56:56');
INSERT INTO `admin_operation_log` VALUES ('95', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:56:56', '2019-02-14 15:56:56');
INSERT INTO `admin_operation_log` VALUES ('96', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"4\",\"name\":\"\\u4e09\\u4e94\\u597d\\u53cb\\u54c1\\u8336\\u8c08\\u4eba\\u751f\",\"sort\":\"0\",\"specify\":\"A\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:57:21', '2019-02-14 15:57:21');
INSERT INTO `admin_operation_log` VALUES ('97', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:57:21', '2019-02-14 15:57:21');
INSERT INTO `admin_operation_log` VALUES ('98', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"4\",\"name\":\"\\u6210\\u7fa4\\u7ed3\\u961f\\u9152\\u5427\\u591c\\u751f\\u6d3b\",\"sort\":\"1\",\"specify\":\"B\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"after-save\":\"2\"}', '2019-02-14 15:57:49', '2019-02-14 15:57:49');
INSERT INTO `admin_operation_log` VALUES ('99', '1', 'admin/answer/create', 'GET', '127.0.0.1', '[]', '2019-02-14 15:57:50', '2019-02-14 15:57:50');
INSERT INTO `admin_operation_log` VALUES ('100', '1', 'admin/answer', 'POST', '127.0.0.1', '{\"tid\":\"4\",\"name\":\"\\u72ec\\u81ea\\u4e00\\u4eba\\u5728\\u5bb6\\u505a\\u80a5\\u5b85\",\"sort\":\"2\",\"specify\":\"C\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\"}', '2019-02-14 15:58:08', '2019-02-14 15:58:08');
INSERT INTO `admin_operation_log` VALUES ('101', '1', 'admin/answer', 'GET', '127.0.0.1', '[]', '2019-02-14 15:58:08', '2019-02-14 15:58:08');
INSERT INTO `admin_operation_log` VALUES ('102', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:58:12', '2019-02-14 15:58:12');
INSERT INTO `admin_operation_log` VALUES ('103', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"desc\"}}', '2019-02-14 15:58:19', '2019-02-14 15:58:19');
INSERT INTO `admin_operation_log` VALUES ('104', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"}}', '2019-02-14 15:58:25', '2019-02-14 15:58:25');
INSERT INTO `admin_operation_log` VALUES ('105', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"},\"id\":null,\"tid\":\"2\",\"name\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-14 15:58:56', '2019-02-14 15:58:56');
INSERT INTO `admin_operation_log` VALUES ('106', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"},\"id\":null,\"tid\":\"1\",\"name\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-14 15:59:00', '2019-02-14 15:59:00');
INSERT INTO `admin_operation_log` VALUES ('107', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"},\"id\":null,\"tid\":\"3\",\"name\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-14 15:59:05', '2019-02-14 15:59:05');
INSERT INTO `admin_operation_log` VALUES ('108', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:59:26', '2019-02-14 15:59:26');
INSERT INTO `admin_operation_log` VALUES ('109', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:59:34', '2019-02-14 15:59:34');
INSERT INTO `admin_operation_log` VALUES ('110', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:59:37', '2019-02-14 15:59:37');
INSERT INTO `admin_operation_log` VALUES ('111', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 15:59:39', '2019-02-14 15:59:39');
INSERT INTO `admin_operation_log` VALUES ('112', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,A,A\",\"cars\":\"\\u4fdd\\u65f6\\u6377cayenne\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-14 16:02:09', '2019-02-14 16:02:09');
INSERT INTO `admin_operation_log` VALUES ('113', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-14 16:02:09', '2019-02-14 16:02:09');
INSERT INTO `admin_operation_log` VALUES ('114', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:02:51', '2019-02-14 16:02:51');
INSERT INTO `admin_operation_log` VALUES ('115', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,B,A\",\"cars\":\"\\u963f\\u65af\\u987f\\u9a6c\\u4e01RAPIDE S\",\"_token\":\"3gUJ9za3f5mU8IYmF6meVSf1qM438WSKkMMjFcAG\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-14 16:03:24', '2019-02-14 16:03:24');
INSERT INTO `admin_operation_log` VALUES ('116', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-14 16:03:24', '2019-02-14 16:03:24');
INSERT INTO `admin_operation_log` VALUES ('117', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:07:34', '2019-02-14 16:07:34');
INSERT INTO `admin_operation_log` VALUES ('118', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:07:36', '2019-02-14 16:07:36');
INSERT INTO `admin_operation_log` VALUES ('119', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:07:37', '2019-02-14 16:07:37');
INSERT INTO `admin_operation_log` VALUES ('120', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:07:38', '2019-02-14 16:07:38');
INSERT INTO `admin_operation_log` VALUES ('121', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:07:40', '2019-02-14 16:07:40');
INSERT INTO `admin_operation_log` VALUES ('122', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:08:42', '2019-02-14 16:08:42');
INSERT INTO `admin_operation_log` VALUES ('123', '1', 'admin/answer/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:08:46', '2019-02-14 16:08:46');
INSERT INTO `admin_operation_log` VALUES ('124', '1', 'admin/answer/1', 'GET', '127.0.0.1', '[]', '2019-02-14 16:10:38', '2019-02-14 16:10:38');
INSERT INTO `admin_operation_log` VALUES ('125', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:10:47', '2019-02-14 16:10:47');
INSERT INTO `admin_operation_log` VALUES ('126', '1', 'admin/answer/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:10:51', '2019-02-14 16:10:51');
INSERT INTO `admin_operation_log` VALUES ('127', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:10:53', '2019-02-14 16:10:53');
INSERT INTO `admin_operation_log` VALUES ('128', '1', 'admin/answer/3', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:10:56', '2019-02-14 16:10:56');
INSERT INTO `admin_operation_log` VALUES ('129', '1', 'admin/answer/3', 'GET', '127.0.0.1', '[]', '2019-02-14 16:12:54', '2019-02-14 16:12:54');
INSERT INTO `admin_operation_log` VALUES ('130', '1', 'admin/answer/3', 'GET', '127.0.0.1', '[]', '2019-02-14 16:17:37', '2019-02-14 16:17:37');
INSERT INTO `admin_operation_log` VALUES ('131', '1', 'admin/answer/3', 'GET', '127.0.0.1', '[]', '2019-02-14 16:18:02', '2019-02-14 16:18:02');
INSERT INTO `admin_operation_log` VALUES ('132', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:18:14', '2019-02-14 16:18:14');
INSERT INTO `admin_operation_log` VALUES ('133', '1', 'admin/answer/6', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:18:17', '2019-02-14 16:18:17');
INSERT INTO `admin_operation_log` VALUES ('134', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:19:51', '2019-02-14 16:19:51');
INSERT INTO `admin_operation_log` VALUES ('135', '1', 'admin/record', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-14 16:35:06', '2019-02-14 16:35:06');
INSERT INTO `admin_operation_log` VALUES ('136', '1', 'admin', 'GET', '127.0.0.1', '[]', '2019-02-15 08:38:10', '2019-02-15 08:38:10');
INSERT INTO `admin_operation_log` VALUES ('137', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:38:17', '2019-02-15 08:38:17');
INSERT INTO `admin_operation_log` VALUES ('138', '1', 'admin/appoint/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:38:23', '2019-02-15 08:38:23');
INSERT INTO `admin_operation_log` VALUES ('139', '1', 'admin/appoint/2', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,B,A\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:38:43', '2019-02-15 08:38:43');
INSERT INTO `admin_operation_log` VALUES ('140', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:38:43', '2019-02-15 08:38:43');
INSERT INTO `admin_operation_log` VALUES ('141', '1', 'admin/appoint/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:38:58', '2019-02-15 08:38:58');
INSERT INTO `admin_operation_log` VALUES ('142', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:39:01', '2019-02-15 08:39:01');
INSERT INTO `admin_operation_log` VALUES ('143', '1', 'admin/appoint/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:39:04', '2019-02-15 08:39:04');
INSERT INTO `admin_operation_log` VALUES ('144', '1', 'admin/appoint/2', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,A,B\",\"cars\":\"\\u672c\\u7530crv\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:39:18', '2019-02-15 08:39:18');
INSERT INTO `admin_operation_log` VALUES ('145', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:39:18', '2019-02-15 08:39:18');
INSERT INTO `admin_operation_log` VALUES ('146', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:39:31', '2019-02-15 08:39:31');
INSERT INTO `admin_operation_log` VALUES ('147', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,A,C\",\"cars\":\"\\u542f\\u8fb0T90\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:39:41', '2019-02-15 08:39:41');
INSERT INTO `admin_operation_log` VALUES ('148', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:39:41', '2019-02-15 08:39:41');
INSERT INTO `admin_operation_log` VALUES ('149', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:39:55', '2019-02-15 08:39:55');
INSERT INTO `admin_operation_log` VALUES ('150', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,B,A\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u83f2\\u65af\\u5854\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:40:03', '2019-02-15 08:40:03');
INSERT INTO `admin_operation_log` VALUES ('151', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:40:03', '2019-02-15 08:40:03');
INSERT INTO `admin_operation_log` VALUES ('152', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:40:17', '2019-02-15 08:40:17');
INSERT INTO `admin_operation_log` VALUES ('153', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,B,B\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:40:35', '2019-02-15 08:40:35');
INSERT INTO `admin_operation_log` VALUES ('154', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:40:35', '2019-02-15 08:40:35');
INSERT INTO `admin_operation_log` VALUES ('155', '1', 'admin/appoint/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:41:09', '2019-02-15 08:41:09');
INSERT INTO `admin_operation_log` VALUES ('156', '1', 'admin/appoint/1', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,A,A\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:41:15', '2019-02-15 08:41:15');
INSERT INTO `admin_operation_log` VALUES ('157', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:41:15', '2019-02-15 08:41:15');
INSERT INTO `admin_operation_log` VALUES ('158', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:41:58', '2019-02-15 08:41:58');
INSERT INTO `admin_operation_log` VALUES ('159', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,B,C\",\"cars\":\"\\u542f\\u8fb0T90\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:42:08', '2019-02-15 08:42:08');
INSERT INTO `admin_operation_log` VALUES ('160', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:42:08', '2019-02-15 08:42:08');
INSERT INTO `admin_operation_log` VALUES ('161', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:42:25', '2019-02-15 08:42:25');
INSERT INTO `admin_operation_log` VALUES ('162', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,C,A\",\"cars\":\"\\u672c\\u7530crv\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:42:39', '2019-02-15 08:42:39');
INSERT INTO `admin_operation_log` VALUES ('163', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:42:39', '2019-02-15 08:42:39');
INSERT INTO `admin_operation_log` VALUES ('164', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:42:57', '2019-02-15 08:42:57');
INSERT INTO `admin_operation_log` VALUES ('165', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,C,B\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:43:10', '2019-02-15 08:43:10');
INSERT INTO `admin_operation_log` VALUES ('166', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:43:10', '2019-02-15 08:43:10');
INSERT INTO `admin_operation_log` VALUES ('167', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:43:27', '2019-02-15 08:43:27');
INSERT INTO `admin_operation_log` VALUES ('168', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,C,C\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:43:37', '2019-02-15 08:43:37');
INSERT INTO `admin_operation_log` VALUES ('169', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:43:38', '2019-02-15 08:43:38');
INSERT INTO `admin_operation_log` VALUES ('170', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:43:53', '2019-02-15 08:43:53');
INSERT INTO `admin_operation_log` VALUES ('171', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,D,A\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:44:01', '2019-02-15 08:44:01');
INSERT INTO `admin_operation_log` VALUES ('172', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:44:01', '2019-02-15 08:44:01');
INSERT INTO `admin_operation_log` VALUES ('173', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:44:12', '2019-02-15 08:44:12');
INSERT INTO `admin_operation_log` VALUES ('174', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,D,B\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:44:22', '2019-02-15 08:44:22');
INSERT INTO `admin_operation_log` VALUES ('175', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:44:22', '2019-02-15 08:44:22');
INSERT INTO `admin_operation_log` VALUES ('176', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:44:25', '2019-02-15 08:44:25');
INSERT INTO `admin_operation_log` VALUES ('177', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,A,D,C\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:44:42', '2019-02-15 08:44:42');
INSERT INTO `admin_operation_log` VALUES ('178', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:44:42', '2019-02-15 08:44:42');
INSERT INTO `admin_operation_log` VALUES ('179', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:44:52', '2019-02-15 08:44:52');
INSERT INTO `admin_operation_log` VALUES ('180', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,A,A\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:45:01', '2019-02-15 08:45:01');
INSERT INTO `admin_operation_log` VALUES ('181', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:45:01', '2019-02-15 08:45:01');
INSERT INTO `admin_operation_log` VALUES ('182', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:45:11', '2019-02-15 08:45:11');
INSERT INTO `admin_operation_log` VALUES ('183', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,A,B\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:45:30', '2019-02-15 08:45:30');
INSERT INTO `admin_operation_log` VALUES ('184', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:45:30', '2019-02-15 08:45:30');
INSERT INTO `admin_operation_log` VALUES ('185', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:45:40', '2019-02-15 08:45:40');
INSERT INTO `admin_operation_log` VALUES ('186', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,A,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u6602\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:45:51', '2019-02-15 08:45:51');
INSERT INTO `admin_operation_log` VALUES ('187', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:45:51', '2019-02-15 08:45:51');
INSERT INTO `admin_operation_log` VALUES ('188', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:46:02', '2019-02-15 08:46:02');
INSERT INTO `admin_operation_log` VALUES ('189', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,B,A\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:46:11', '2019-02-15 08:46:11');
INSERT INTO `admin_operation_log` VALUES ('190', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:46:11', '2019-02-15 08:46:11');
INSERT INTO `admin_operation_log` VALUES ('191', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:46:20', '2019-02-15 08:46:20');
INSERT INTO `admin_operation_log` VALUES ('192', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,B,B\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:46:31', '2019-02-15 08:46:31');
INSERT INTO `admin_operation_log` VALUES ('193', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:46:31', '2019-02-15 08:46:31');
INSERT INTO `admin_operation_log` VALUES ('194', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:46:42', '2019-02-15 08:46:42');
INSERT INTO `admin_operation_log` VALUES ('195', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,B,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u6602\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:46:54', '2019-02-15 08:46:54');
INSERT INTO `admin_operation_log` VALUES ('196', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:46:54', '2019-02-15 08:46:54');
INSERT INTO `admin_operation_log` VALUES ('197', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:47:08', '2019-02-15 08:47:08');
INSERT INTO `admin_operation_log` VALUES ('198', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,C,A\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:47:21', '2019-02-15 08:47:21');
INSERT INTO `admin_operation_log` VALUES ('199', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:47:21', '2019-02-15 08:47:21');
INSERT INTO `admin_operation_log` VALUES ('200', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,C,B\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\"}', '2019-02-15 08:47:40', '2019-02-15 08:47:40');
INSERT INTO `admin_operation_log` VALUES ('201', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:47:40', '2019-02-15 08:47:40');
INSERT INTO `admin_operation_log` VALUES ('202', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:47:48', '2019-02-15 08:47:48');
INSERT INTO `admin_operation_log` VALUES ('203', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,C,C\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:48:03', '2019-02-15 08:48:03');
INSERT INTO `admin_operation_log` VALUES ('204', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:48:03', '2019-02-15 08:48:03');
INSERT INTO `admin_operation_log` VALUES ('205', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:48:16', '2019-02-15 08:48:16');
INSERT INTO `admin_operation_log` VALUES ('206', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,D,A\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:48:31', '2019-02-15 08:48:31');
INSERT INTO `admin_operation_log` VALUES ('207', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:48:32', '2019-02-15 08:48:32');
INSERT INTO `admin_operation_log` VALUES ('208', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:48:34', '2019-02-15 08:48:34');
INSERT INTO `admin_operation_log` VALUES ('209', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,D,B\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:48:51', '2019-02-15 08:48:51');
INSERT INTO `admin_operation_log` VALUES ('210', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:48:51', '2019-02-15 08:48:51');
INSERT INTO `admin_operation_log` VALUES ('211', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:49:04', '2019-02-15 08:49:04');
INSERT INTO `admin_operation_log` VALUES ('212', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"A,B,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:49:14', '2019-02-15 08:49:14');
INSERT INTO `admin_operation_log` VALUES ('213', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:49:15', '2019-02-15 08:49:15');
INSERT INTO `admin_operation_log` VALUES ('214', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:49:33', '2019-02-15 08:49:33');
INSERT INTO `admin_operation_log` VALUES ('215', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,A,A\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:49:46', '2019-02-15 08:49:46');
INSERT INTO `admin_operation_log` VALUES ('216', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:49:46', '2019-02-15 08:49:46');
INSERT INTO `admin_operation_log` VALUES ('217', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:49:50', '2019-02-15 08:49:50');
INSERT INTO `admin_operation_log` VALUES ('218', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,A,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:50:18', '2019-02-15 08:50:18');
INSERT INTO `admin_operation_log` VALUES ('219', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:50:18', '2019-02-15 08:50:18');
INSERT INTO `admin_operation_log` VALUES ('220', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,B,A\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:50:38', '2019-02-15 08:50:38');
INSERT INTO `admin_operation_log` VALUES ('221', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:50:38', '2019-02-15 08:50:38');
INSERT INTO `admin_operation_log` VALUES ('222', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,C,A\",\"cars\":\"\\u6797\\u80afMKC\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:50:53', '2019-02-15 08:50:53');
INSERT INTO `admin_operation_log` VALUES ('223', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:50:53', '2019-02-15 08:50:53');
INSERT INTO `admin_operation_log` VALUES ('224', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,D,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\"}', '2019-02-15 08:51:12', '2019-02-15 08:51:12');
INSERT INTO `admin_operation_log` VALUES ('225', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:51:12', '2019-02-15 08:51:12');
INSERT INTO `admin_operation_log` VALUES ('226', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:51:21', '2019-02-15 08:51:21');
INSERT INTO `admin_operation_log` VALUES ('227', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,A,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:51:35', '2019-02-15 08:51:35');
INSERT INTO `admin_operation_log` VALUES ('228', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:51:36', '2019-02-15 08:51:36');
INSERT INTO `admin_operation_log` VALUES ('229', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,A,C\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u9886\\u52a8\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:51:50', '2019-02-15 08:51:50');
INSERT INTO `admin_operation_log` VALUES ('230', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:51:50', '2019-02-15 08:51:50');
INSERT INTO `admin_operation_log` VALUES ('231', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,B,A\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u9886\\u52a8\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\"}', '2019-02-15 08:52:07', '2019-02-15 08:52:07');
INSERT INTO `admin_operation_log` VALUES ('232', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:52:07', '2019-02-15 08:52:07');
INSERT INTO `admin_operation_log` VALUES ('233', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:52:16', '2019-02-15 08:52:16');
INSERT INTO `admin_operation_log` VALUES ('234', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,C,A\",\"cars\":\"\\u6797\\u80afMKC\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:52:23', '2019-02-15 08:52:23');
INSERT INTO `admin_operation_log` VALUES ('235', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:52:23', '2019-02-15 08:52:23');
INSERT INTO `admin_operation_log` VALUES ('236', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,D,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:52:45', '2019-02-15 08:52:45');
INSERT INTO `admin_operation_log` VALUES ('237', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:52:45', '2019-02-15 08:52:45');
INSERT INTO `admin_operation_log` VALUES ('238', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,A,B\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u5cb3\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:52:57', '2019-02-15 08:52:57');
INSERT INTO `admin_operation_log` VALUES ('239', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:52:57', '2019-02-15 08:52:57');
INSERT INTO `admin_operation_log` VALUES ('240', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,A,C\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\"}', '2019-02-15 08:53:11', '2019-02-15 08:53:11');
INSERT INTO `admin_operation_log` VALUES ('241', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:53:11', '2019-02-15 08:53:11');
INSERT INTO `admin_operation_log` VALUES ('242', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:53:13', '2019-02-15 08:53:13');
INSERT INTO `admin_operation_log` VALUES ('243', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT4\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-15 08:53:28', '2019-02-15 08:53:28');
INSERT INTO `admin_operation_log` VALUES ('244', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:53:29', '2019-02-15 08:53:29');
INSERT INTO `admin_operation_log` VALUES ('245', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,C,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u5cb3\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:53:42', '2019-02-15 08:53:42');
INSERT INTO `admin_operation_log` VALUES ('246', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:53:42', '2019-02-15 08:53:42');
INSERT INTO `admin_operation_log` VALUES ('247', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,B,B\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:53:54', '2019-02-15 08:53:54');
INSERT INTO `admin_operation_log` VALUES ('248', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:53:54', '2019-02-15 08:53:54');
INSERT INTO `admin_operation_log` VALUES ('249', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,B,C\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:54:07', '2019-02-15 08:54:07');
INSERT INTO `admin_operation_log` VALUES ('250', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:54:08', '2019-02-15 08:54:08');
INSERT INTO `admin_operation_log` VALUES ('251', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,C,B\",\"cars\":\"\\u6797\\u80afMKC\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:54:23', '2019-02-15 08:54:23');
INSERT INTO `admin_operation_log` VALUES ('252', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:54:23', '2019-02-15 08:54:23');
INSERT INTO `admin_operation_log` VALUES ('253', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,B,D,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:54:54', '2019-02-15 08:54:54');
INSERT INTO `admin_operation_log` VALUES ('254', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:54:54', '2019-02-15 08:54:54');
INSERT INTO `admin_operation_log` VALUES ('255', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,B,B\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:55:13', '2019-02-15 08:55:13');
INSERT INTO `admin_operation_log` VALUES ('256', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:55:13', '2019-02-15 08:55:13');
INSERT INTO `admin_operation_log` VALUES ('257', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,B,C\",\"cars\":\"\\u65e5\\u4ea7\\u9a90\\u8fbe\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:55:27', '2019-02-15 08:55:27');
INSERT INTO `admin_operation_log` VALUES ('258', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:55:27', '2019-02-15 08:55:27');
INSERT INTO `admin_operation_log` VALUES ('259', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,C,B\",\"cars\":\"\\u6797\\u80afMKC\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:55:42', '2019-02-15 08:55:42');
INSERT INTO `admin_operation_log` VALUES ('260', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:55:42', '2019-02-15 08:55:42');
INSERT INTO `admin_operation_log` VALUES ('261', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,C,C\",\"cars\":\"\\u6797\\u80afMKC\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:55:56', '2019-02-15 08:55:56');
INSERT INTO `admin_operation_log` VALUES ('262', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:55:56', '2019-02-15 08:55:56');
INSERT INTO `admin_operation_log` VALUES ('263', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,D,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"after-save\":\"2\"}', '2019-02-15 08:56:12', '2019-02-15 08:56:12');
INSERT INTO `admin_operation_log` VALUES ('264', '1', 'admin/appoint/create', 'GET', '127.0.0.1', '[]', '2019-02-15 08:56:12', '2019-02-15 08:56:12');
INSERT INTO `admin_operation_log` VALUES ('265', '1', 'admin/appoint', 'POST', '127.0.0.1', '{\"specify\":\"B,A,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT4\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\"}', '2019-02-15 08:56:35', '2019-02-15 08:56:35');
INSERT INTO `admin_operation_log` VALUES ('266', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-15 08:56:35', '2019-02-15 08:56:35');
INSERT INTO `admin_operation_log` VALUES ('267', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,A,D,C\",\"cars\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:56:53', '2019-02-15 08:56:53');
INSERT INTO `admin_operation_log` VALUES ('268', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:57:12', '2019-02-15 08:57:12');
INSERT INTO `admin_operation_log` VALUES ('269', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:57:27', '2019-02-15 08:57:27');
INSERT INTO `admin_operation_log` VALUES ('270', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"desc\"}}', '2019-02-15 08:57:34', '2019-02-15 08:57:34');
INSERT INTO `admin_operation_log` VALUES ('271', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"}}', '2019-02-15 08:57:35', '2019-02-15 08:57:35');
INSERT INTO `admin_operation_log` VALUES ('272', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:58:20', '2019-02-15 08:58:20');
INSERT INTO `admin_operation_log` VALUES ('273', '1', 'admin/topic/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:58:23', '2019-02-15 08:58:23');
INSERT INTO `admin_operation_log` VALUES ('274', '1', 'admin/topic/1', 'PUT', '127.0.0.1', '{\"name\":\"\\u6027\\u522b\\uff1f\",\"sort\":\"0\",\"_token\":\"HrzdC4vXEfT1F4JDJE0VuFUrdKEo78hQVQUpetMK\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/topic\"}', '2019-02-15 08:58:31', '2019-02-15 08:58:31');
INSERT INTO `admin_operation_log` VALUES ('275', '1', 'admin/topic', 'GET', '127.0.0.1', '[]', '2019-02-15 08:58:31', '2019-02-15 08:58:31');
INSERT INTO `admin_operation_log` VALUES ('276', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 08:58:34', '2019-02-15 08:58:34');
INSERT INTO `admin_operation_log` VALUES ('277', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"desc\"}}', '2019-02-15 08:58:37', '2019-02-15 08:58:37');
INSERT INTO `admin_operation_log` VALUES ('278', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"id\",\"type\":\"asc\"}}', '2019-02-15 08:58:38', '2019-02-15 08:58:38');
INSERT INTO `admin_operation_log` VALUES ('279', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_sort\":{\"column\":\"id\",\"type\":\"asc\"}}', '2019-02-15 09:01:18', '2019-02-15 09:01:18');
INSERT INTO `admin_operation_log` VALUES ('280', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 09:01:22', '2019-02-15 09:01:22');
INSERT INTO `admin_operation_log` VALUES ('281', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 09:01:38', '2019-02-15 09:01:38');
INSERT INTO `admin_operation_log` VALUES ('282', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 09:01:39', '2019-02-15 09:01:39');
INSERT INTO `admin_operation_log` VALUES ('283', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 09:01:44', '2019-02-15 09:01:44');
INSERT INTO `admin_operation_log` VALUES ('284', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-15 09:01:45', '2019-02-15 09:01:45');
INSERT INTO `admin_operation_log` VALUES ('285', '1', 'admin/answer', 'GET', '127.0.0.1', '[]', '2019-02-15 09:02:24', '2019-02-15 09:02:24');
INSERT INTO `admin_operation_log` VALUES ('286', '1', 'admin/answer', 'GET', '127.0.0.1', '[]', '2019-02-15 09:02:47', '2019-02-15 09:02:47');
INSERT INTO `admin_operation_log` VALUES ('287', '1', 'admin', 'GET', '127.0.0.1', '[]', '2019-02-18 14:07:59', '2019-02-18 14:07:59');
INSERT INTO `admin_operation_log` VALUES ('288', '1', 'admin/topic', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:08:18', '2019-02-18 14:08:18');
INSERT INTO `admin_operation_log` VALUES ('289', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:09:32', '2019-02-18 14:09:32');
INSERT INTO `admin_operation_log` VALUES ('290', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"100\"}', '2019-02-18 14:09:39', '2019-02-18 14:09:39');
INSERT INTO `admin_operation_log` VALUES ('291', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:09:43', '2019-02-18 14:09:43');
INSERT INTO `admin_operation_log` VALUES ('292', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"30\"}', '2019-02-18 14:09:47', '2019-02-18 14:09:47');
INSERT INTO `admin_operation_log` VALUES ('293', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"50\"}', '2019-02-18 14:09:50', '2019-02-18 14:09:50');
INSERT INTO `admin_operation_log` VALUES ('294', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:10:17', '2019-02-18 14:10:17');
INSERT INTO `admin_operation_log` VALUES ('295', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:13:36', '2019-02-18 14:13:36');
INSERT INTO `admin_operation_log` VALUES ('296', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:15:02', '2019-02-18 14:15:02');
INSERT INTO `admin_operation_log` VALUES ('297', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"vehicle\",\"model_name\":\"App\\\\Models\\\\VehicleModels\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\VehicleController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u8f66\\u578b\\u540d\\u79f0\"},{\"name\":\"imgurl\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":\"\\u8f66\\u578b\\u56fe\\u7247\"}],\"primary_key\":\"id\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\"}', '2019-02-18 14:17:29', '2019-02-18 14:17:29');
INSERT INTO `admin_operation_log` VALUES ('298', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2019-02-18 14:17:32', '2019-02-18 14:17:32');
INSERT INTO `admin_operation_log` VALUES ('299', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:17:40', '2019-02-18 14:17:40');
INSERT INTO `admin_operation_log` VALUES ('300', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u8f66\\u578b\",\"icon\":\"fa-bars\",\"uri\":\"vehicle\",\"roles\":[null],\"permission\":null,\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\"}', '2019-02-18 14:18:06', '2019-02-18 14:18:06');
INSERT INTO `admin_operation_log` VALUES ('301', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-18 14:18:06', '2019-02-18 14:18:06');
INSERT INTO `admin_operation_log` VALUES ('302', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-18 14:18:09', '2019-02-18 14:18:09');
INSERT INTO `admin_operation_log` VALUES ('303', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:18:25', '2019-02-18 14:18:25');
INSERT INTO `admin_operation_log` VALUES ('304', '1', 'admin/vehicle', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:26:16', '2019-02-18 14:26:16');
INSERT INTO `admin_operation_log` VALUES ('305', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:26:37', '2019-02-18 14:26:37');
INSERT INTO `admin_operation_log` VALUES ('306', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:29:48', '2019-02-18 14:29:48');
INSERT INTO `admin_operation_log` VALUES ('307', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:29:51', '2019-02-18 14:29:51');
INSERT INTO `admin_operation_log` VALUES ('308', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:29:54', '2019-02-18 14:29:54');
INSERT INTO `admin_operation_log` VALUES ('309', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"title\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u83f2\\u65af\\u5854\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\"}', '2019-02-18 14:30:32', '2019-02-18 14:30:32');
INSERT INTO `admin_operation_log` VALUES ('310', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:30:34', '2019-02-18 14:30:34');
INSERT INTO `admin_operation_log` VALUES ('311', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"title\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u83f2\\u65af\\u5854\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\"}', '2019-02-18 14:31:26', '2019-02-18 14:31:26');
INSERT INTO `admin_operation_log` VALUES ('312', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 14:31:26', '2019-02-18 14:31:26');
INSERT INTO `admin_operation_log` VALUES ('313', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 14:31:45', '2019-02-18 14:31:45');
INSERT INTO `admin_operation_log` VALUES ('314', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 14:32:29', '2019-02-18 14:32:29');
INSERT INTO `admin_operation_log` VALUES ('315', '1', 'admin/vehicle/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:32:57', '2019-02-18 14:32:57');
INSERT INTO `admin_operation_log` VALUES ('316', '1', 'admin/vehicle/1', 'PUT', '127.0.0.1', '{\"name\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u83f2\\u65af\\u5854\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/vehicle\"}', '2019-02-18 14:33:00', '2019-02-18 14:33:00');
INSERT INTO `admin_operation_log` VALUES ('317', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 14:33:00', '2019-02-18 14:33:00');
INSERT INTO `admin_operation_log` VALUES ('318', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:33:14', '2019-02-18 14:33:14');
INSERT INTO `admin_operation_log` VALUES ('319', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u9886\\u52a8\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/vehicle\"}', '2019-02-18 14:33:25', '2019-02-18 14:33:25');
INSERT INTO `admin_operation_log` VALUES ('320', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:33:25', '2019-02-18 14:33:25');
INSERT INTO `admin_operation_log` VALUES ('321', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u672c\\u7530crv\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:33:34', '2019-02-18 14:33:34');
INSERT INTO `admin_operation_log` VALUES ('322', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:33:34', '2019-02-18 14:33:34');
INSERT INTO `admin_operation_log` VALUES ('323', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u672c\\u7530\\u601d\\u57df\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:33:45', '2019-02-18 14:33:45');
INSERT INTO `admin_operation_log` VALUES ('324', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:33:45', '2019-02-18 14:33:45');
INSERT INTO `admin_operation_log` VALUES ('325', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u52b2\\u70ab\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:33:58', '2019-02-18 14:33:58');
INSERT INTO `admin_operation_log` VALUES ('326', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:33:58', '2019-02-18 14:33:58');
INSERT INTO `admin_operation_log` VALUES ('327', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:07', '2019-02-18 14:34:07');
INSERT INTO `admin_operation_log` VALUES ('328', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:07', '2019-02-18 14:34:07');
INSERT INTO `admin_operation_log` VALUES ('329', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u51ef\\u8fea\\u62c9\\u514bXT4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:17', '2019-02-18 14:34:17');
INSERT INTO `admin_operation_log` VALUES ('330', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:17', '2019-02-18 14:34:17');
INSERT INTO `admin_operation_log` VALUES ('331', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u51ef\\u8fea\\u62c9\\u514bXT50\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:27', '2019-02-18 14:34:27');
INSERT INTO `admin_operation_log` VALUES ('332', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:27', '2019-02-18 14:34:27');
INSERT INTO `admin_operation_log` VALUES ('333', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u6797\\u80afMKC\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:37', '2019-02-18 14:34:37');
INSERT INTO `admin_operation_log` VALUES ('334', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:38', '2019-02-18 14:34:38');
INSERT INTO `admin_operation_log` VALUES ('335', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:48', '2019-02-18 14:34:48');
INSERT INTO `admin_operation_log` VALUES ('336', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:48', '2019-02-18 14:34:48');
INSERT INTO `admin_operation_log` VALUES ('337', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u8def\\u864e\\u6781\\u5149\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:34:56', '2019-02-18 14:34:56');
INSERT INTO `admin_operation_log` VALUES ('338', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:34:56', '2019-02-18 14:34:56');
INSERT INTO `admin_operation_log` VALUES ('339', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:35:05', '2019-02-18 14:35:05');
INSERT INTO `admin_operation_log` VALUES ('340', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:05', '2019-02-18 14:35:05');
INSERT INTO `admin_operation_log` VALUES ('341', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u542f\\u8fb0T60\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:35:16', '2019-02-18 14:35:16');
INSERT INTO `admin_operation_log` VALUES ('342', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:16', '2019-02-18 14:35:16');
INSERT INTO `admin_operation_log` VALUES ('343', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u542f\\u8fb0T90\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:35:27', '2019-02-18 14:35:27');
INSERT INTO `admin_operation_log` VALUES ('344', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:28', '2019-02-18 14:35:28');
INSERT INTO `admin_operation_log` VALUES ('345', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u65e5\\u4ea7\\u9a90\\u8fbe\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:35:37', '2019-02-18 14:35:37');
INSERT INTO `admin_operation_log` VALUES ('346', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:37', '2019-02-18 14:35:37');
INSERT INTO `admin_operation_log` VALUES ('347', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u6602\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"after-save\":\"2\"}', '2019-02-18 14:35:46', '2019-02-18 14:35:46');
INSERT INTO `admin_operation_log` VALUES ('348', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:46', '2019-02-18 14:35:46');
INSERT INTO `admin_operation_log` VALUES ('349', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u5cb3\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\"}', '2019-02-18 14:35:54', '2019-02-18 14:35:54');
INSERT INTO `admin_operation_log` VALUES ('350', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 14:35:54', '2019-02-18 14:35:54');
INSERT INTO `admin_operation_log` VALUES ('351', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:36:38', '2019-02-18 14:36:38');
INSERT INTO `admin_operation_log` VALUES ('352', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:46:59', '2019-02-18 14:46:59');
INSERT INTO `admin_operation_log` VALUES ('353', '1', 'admin/appoint/48/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:47:06', '2019-02-18 14:47:06');
INSERT INTO `admin_operation_log` VALUES ('354', '1', 'admin/appoint/48', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT4\",\"vid\":\"7\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:48:44', '2019-02-18 14:48:44');
INSERT INTO `admin_operation_log` VALUES ('355', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:48:44', '2019-02-18 14:48:44');
INSERT INTO `admin_operation_log` VALUES ('356', '1', 'admin/appoint/47/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:48:48', '2019-02-18 14:48:48');
INSERT INTO `admin_operation_log` VALUES ('357', '1', 'admin/appoint/47', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,D,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:48:56', '2019-02-18 14:48:56');
INSERT INTO `admin_operation_log` VALUES ('358', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:48:56', '2019-02-18 14:48:56');
INSERT INTO `admin_operation_log` VALUES ('359', '1', 'admin/appoint/46/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:48:59', '2019-02-18 14:48:59');
INSERT INTO `admin_operation_log` VALUES ('360', '1', 'admin/appoint/46', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,C,C\",\"cars\":\"\\u6797\\u80afMKC\",\"vid\":\"9\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:08', '2019-02-18 14:49:08');
INSERT INTO `admin_operation_log` VALUES ('361', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:08', '2019-02-18 14:49:08');
INSERT INTO `admin_operation_log` VALUES ('362', '1', 'admin/appoint/45/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:11', '2019-02-18 14:49:11');
INSERT INTO `admin_operation_log` VALUES ('363', '1', 'admin/appoint/45', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,C,B\",\"cars\":\"\\u6797\\u80afMKC\",\"vid\":\"9\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:15', '2019-02-18 14:49:15');
INSERT INTO `admin_operation_log` VALUES ('364', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:15', '2019-02-18 14:49:15');
INSERT INTO `admin_operation_log` VALUES ('365', '1', 'admin/appoint/44/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:19', '2019-02-18 14:49:19');
INSERT INTO `admin_operation_log` VALUES ('366', '1', 'admin/appoint/44', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,B,C\",\"cars\":\"\\u65e5\\u4ea7\\u9a90\\u8fbe\",\"vid\":\"15\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:25', '2019-02-18 14:49:25');
INSERT INTO `admin_operation_log` VALUES ('367', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:25', '2019-02-18 14:49:25');
INSERT INTO `admin_operation_log` VALUES ('368', '1', 'admin/appoint/43/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:28', '2019-02-18 14:49:28');
INSERT INTO `admin_operation_log` VALUES ('369', '1', 'admin/appoint/43', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,B,B\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:33', '2019-02-18 14:49:33');
INSERT INTO `admin_operation_log` VALUES ('370', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:33', '2019-02-18 14:49:33');
INSERT INTO `admin_operation_log` VALUES ('371', '1', 'admin/appoint/42/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:36', '2019-02-18 14:49:36');
INSERT INTO `admin_operation_log` VALUES ('372', '1', 'admin/appoint/42', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,D,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:43', '2019-02-18 14:49:43');
INSERT INTO `admin_operation_log` VALUES ('373', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:43', '2019-02-18 14:49:43');
INSERT INTO `admin_operation_log` VALUES ('374', '1', 'admin/appoint/41/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:46', '2019-02-18 14:49:46');
INSERT INTO `admin_operation_log` VALUES ('375', '1', 'admin/appoint/41', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,C,B\",\"cars\":\"\\u6797\\u80afMKC\",\"vid\":\"9\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:49', '2019-02-18 14:49:49');
INSERT INTO `admin_operation_log` VALUES ('376', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:49', '2019-02-18 14:49:49');
INSERT INTO `admin_operation_log` VALUES ('377', '1', 'admin/appoint/40/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:52', '2019-02-18 14:49:52');
INSERT INTO `admin_operation_log` VALUES ('378', '1', 'admin/appoint/40', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,B,C\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:49:55', '2019-02-18 14:49:55');
INSERT INTO `admin_operation_log` VALUES ('379', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:49:55', '2019-02-18 14:49:55');
INSERT INTO `admin_operation_log` VALUES ('380', '1', 'admin/appoint/39/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:49:58', '2019-02-18 14:49:58');
INSERT INTO `admin_operation_log` VALUES ('381', '1', 'admin/appoint/39', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,B,B\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:02', '2019-02-18 14:50:02');
INSERT INTO `admin_operation_log` VALUES ('382', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:02', '2019-02-18 14:50:02');
INSERT INTO `admin_operation_log` VALUES ('383', '1', 'admin/appoint/38/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:50:05', '2019-02-18 14:50:05');
INSERT INTO `admin_operation_log` VALUES ('384', '1', 'admin/appoint/38', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,C,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u5cb3\",\"vid\":\"17\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:21', '2019-02-18 14:50:21');
INSERT INTO `admin_operation_log` VALUES ('385', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:22', '2019-02-18 14:50:22');
INSERT INTO `admin_operation_log` VALUES ('386', '1', 'admin/appoint/37/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:50:25', '2019-02-18 14:50:25');
INSERT INTO `admin_operation_log` VALUES ('387', '1', 'admin/appoint/37', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT4\",\"vid\":\"7\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:33', '2019-02-18 14:50:33');
INSERT INTO `admin_operation_log` VALUES ('388', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:34', '2019-02-18 14:50:34');
INSERT INTO `admin_operation_log` VALUES ('389', '1', 'admin/appoint/36/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:50:36', '2019-02-18 14:50:36');
INSERT INTO `admin_operation_log` VALUES ('390', '1', 'admin/appoint/36', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,A,C\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:42', '2019-02-18 14:50:42');
INSERT INTO `admin_operation_log` VALUES ('391', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:43', '2019-02-18 14:50:43');
INSERT INTO `admin_operation_log` VALUES ('392', '1', 'admin/appoint/35/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:50:45', '2019-02-18 14:50:45');
INSERT INTO `admin_operation_log` VALUES ('393', '1', 'admin/appoint/35', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,A,B\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u5cb3\",\"vid\":\"17\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:50', '2019-02-18 14:50:50');
INSERT INTO `admin_operation_log` VALUES ('394', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:50', '2019-02-18 14:50:50');
INSERT INTO `admin_operation_log` VALUES ('395', '1', 'admin/appoint/34/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:50:53', '2019-02-18 14:50:53');
INSERT INTO `admin_operation_log` VALUES ('396', '1', 'admin/appoint/34', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,D,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:50:58', '2019-02-18 14:50:58');
INSERT INTO `admin_operation_log` VALUES ('397', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:50:58', '2019-02-18 14:50:58');
INSERT INTO `admin_operation_log` VALUES ('398', '1', 'admin/appoint/33/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:02', '2019-02-18 14:51:02');
INSERT INTO `admin_operation_log` VALUES ('399', '1', 'admin/appoint/33', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,C,A\",\"cars\":\"\\u6797\\u80afMKC\",\"vid\":\"9\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:51:06', '2019-02-18 14:51:06');
INSERT INTO `admin_operation_log` VALUES ('400', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:51:06', '2019-02-18 14:51:06');
INSERT INTO `admin_operation_log` VALUES ('401', '1', 'admin/appoint/32/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:09', '2019-02-18 14:51:09');
INSERT INTO `admin_operation_log` VALUES ('402', '1', 'admin/appoint/32', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,B,A\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u9886\\u52a8\",\"vid\":\"2\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:51:17', '2019-02-18 14:51:17');
INSERT INTO `admin_operation_log` VALUES ('403', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:51:17', '2019-02-18 14:51:17');
INSERT INTO `admin_operation_log` VALUES ('404', '1', 'admin/appoint/31/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:21', '2019-02-18 14:51:21');
INSERT INTO `admin_operation_log` VALUES ('405', '1', 'admin/appoint/31', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,A,C\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u9886\\u52a8\",\"vid\":\"2\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:51:25', '2019-02-18 14:51:25');
INSERT INTO `admin_operation_log` VALUES ('406', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:51:25', '2019-02-18 14:51:25');
INSERT INTO `admin_operation_log` VALUES ('407', '1', 'admin/appoint/30/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:27', '2019-02-18 14:51:27');
INSERT INTO `admin_operation_log` VALUES ('408', '1', 'admin/appoint/30', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,A,B\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:51:33', '2019-02-18 14:51:33');
INSERT INTO `admin_operation_log` VALUES ('409', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:51:33', '2019-02-18 14:51:33');
INSERT INTO `admin_operation_log` VALUES ('410', '1', 'admin/appoint/29/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:35', '2019-02-18 14:51:35');
INSERT INTO `admin_operation_log` VALUES ('411', '1', 'admin/appoint/29', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,D,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint\"}', '2019-02-18 14:51:42', '2019-02-18 14:51:42');
INSERT INTO `admin_operation_log` VALUES ('412', '1', 'admin/appoint', 'GET', '127.0.0.1', '[]', '2019-02-18 14:51:42', '2019-02-18 14:51:42');
INSERT INTO `admin_operation_log` VALUES ('413', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:44', '2019-02-18 14:51:44');
INSERT INTO `admin_operation_log` VALUES ('414', '1', 'admin/appoint/28/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:47', '2019-02-18 14:51:47');
INSERT INTO `admin_operation_log` VALUES ('415', '1', 'admin/appoint/28', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,C,A\",\"cars\":\"\\u6797\\u80afMKC\",\"vid\":\"9\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:51:52', '2019-02-18 14:51:52');
INSERT INTO `admin_operation_log` VALUES ('416', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:51:52', '2019-02-18 14:51:52');
INSERT INTO `admin_operation_log` VALUES ('417', '1', 'admin/appoint/27/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:51:54', '2019-02-18 14:51:54');
INSERT INTO `admin_operation_log` VALUES ('418', '1', 'admin/appoint/27', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,B,A\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:51:58', '2019-02-18 14:51:58');
INSERT INTO `admin_operation_log` VALUES ('419', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:51:58', '2019-02-18 14:51:58');
INSERT INTO `admin_operation_log` VALUES ('420', '1', 'admin/appoint/26/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:00', '2019-02-18 14:52:00');
INSERT INTO `admin_operation_log` VALUES ('421', '1', 'admin/appoint/26', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,A,A\",\"cars\":\"\\u8def\\u864e\\u6781\\u5149\",\"vid\":\"11\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:52:05', '2019-02-18 14:52:05');
INSERT INTO `admin_operation_log` VALUES ('422', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:52:05', '2019-02-18 14:52:05');
INSERT INTO `admin_operation_log` VALUES ('423', '1', 'admin/appoint/25/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:07', '2019-02-18 14:52:07');
INSERT INTO `admin_operation_log` VALUES ('424', '1', 'admin/appoint/25', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,A,A\",\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":\"4\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:52:11', '2019-02-18 14:52:11');
INSERT INTO `admin_operation_log` VALUES ('425', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:52:11', '2019-02-18 14:52:11');
INSERT INTO `admin_operation_log` VALUES ('426', '1', 'admin/appoint/24/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:13', '2019-02-18 14:52:13');
INSERT INTO `admin_operation_log` VALUES ('427', '1', 'admin/appoint/24', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,D,C\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"vid\":\"8\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:52:27', '2019-02-18 14:52:27');
INSERT INTO `admin_operation_log` VALUES ('428', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:52:27', '2019-02-18 14:52:27');
INSERT INTO `admin_operation_log` VALUES ('429', '1', 'admin/appoint/23/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:30', '2019-02-18 14:52:30');
INSERT INTO `admin_operation_log` VALUES ('430', '1', 'admin/appoint/23', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,D,B\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"vid\":\"10\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:52:35', '2019-02-18 14:52:35');
INSERT INTO `admin_operation_log` VALUES ('431', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:52:35', '2019-02-18 14:52:35');
INSERT INTO `admin_operation_log` VALUES ('432', '1', 'admin/appoint/22/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:37', '2019-02-18 14:52:37');
INSERT INTO `admin_operation_log` VALUES ('433', '1', 'admin/appoint/22', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,D,A\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"vid\":\"12\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:52:43', '2019-02-18 14:52:43');
INSERT INTO `admin_operation_log` VALUES ('434', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:52:43', '2019-02-18 14:52:43');
INSERT INTO `admin_operation_log` VALUES ('435', '1', 'admin/appoint/21/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:52:45', '2019-02-18 14:52:45');
INSERT INTO `admin_operation_log` VALUES ('436', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:54:21', '2019-02-18 14:54:21');
INSERT INTO `admin_operation_log` VALUES ('437', '1', 'admin/appoint/20/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:54:31', '2019-02-18 14:54:31');
INSERT INTO `admin_operation_log` VALUES ('438', '1', 'admin/appoint/20', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,C,B\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"vid\":\"12\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:54:37', '2019-02-18 14:54:37');
INSERT INTO `admin_operation_log` VALUES ('439', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:54:37', '2019-02-18 14:54:37');
INSERT INTO `admin_operation_log` VALUES ('440', '1', 'admin/appoint/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:54:40', '2019-02-18 14:54:40');
INSERT INTO `admin_operation_log` VALUES ('441', '1', 'admin/appoint/19', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,C,A\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"vid\":\"10\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:54:44', '2019-02-18 14:54:44');
INSERT INTO `admin_operation_log` VALUES ('442', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:54:45', '2019-02-18 14:54:45');
INSERT INTO `admin_operation_log` VALUES ('443', '1', 'admin/appoint/18/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:54:47', '2019-02-18 14:54:47');
INSERT INTO `admin_operation_log` VALUES ('444', '1', 'admin/appoint/18', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,B,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u6602\",\"vid\":\"16\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:54:52', '2019-02-18 14:54:52');
INSERT INTO `admin_operation_log` VALUES ('445', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:54:52', '2019-02-18 14:54:52');
INSERT INTO `admin_operation_log` VALUES ('446', '1', 'admin/appoint/17/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:54:55', '2019-02-18 14:54:55');
INSERT INTO `admin_operation_log` VALUES ('447', '1', 'admin/appoint/17', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,B,B\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"vid\":\"8\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:55:01', '2019-02-18 14:55:01');
INSERT INTO `admin_operation_log` VALUES ('448', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:55:01', '2019-02-18 14:55:01');
INSERT INTO `admin_operation_log` VALUES ('449', '1', 'admin/appoint/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:55:04', '2019-02-18 14:55:04');
INSERT INTO `admin_operation_log` VALUES ('450', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:55:07', '2019-02-18 14:55:07');
INSERT INTO `admin_operation_log` VALUES ('451', '1', 'admin/appoint/15/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:55:11', '2019-02-18 14:55:11');
INSERT INTO `admin_operation_log` VALUES ('452', '1', 'admin/appoint/15', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,A,C\",\"cars\":\"\\u4e0a\\u6c7d\\u5927\\u4f17\\u9014\\u6602\",\"vid\":\"16\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:55:16', '2019-02-18 14:55:16');
INSERT INTO `admin_operation_log` VALUES ('453', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:55:16', '2019-02-18 14:55:16');
INSERT INTO `admin_operation_log` VALUES ('454', '1', 'admin/appoint/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:56:12', '2019-02-18 14:56:12');
INSERT INTO `admin_operation_log` VALUES ('455', '1', 'admin/appoint/14', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,A,B\",\"cars\":\"\\u6797\\u80af\\u9886\\u822a\\u5458\",\"vid\":\"10\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:56:17', '2019-02-18 14:56:17');
INSERT INTO `admin_operation_log` VALUES ('456', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:56:17', '2019-02-18 14:56:17');
INSERT INTO `admin_operation_log` VALUES ('457', '1', 'admin/appoint/13/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:56:20', '2019-02-18 14:56:20');
INSERT INTO `admin_operation_log` VALUES ('458', '1', 'admin/appoint/13', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,A,A\",\"cars\":\"\\u8def\\u864e\\u63fd\\u80dc\",\"vid\":\"12\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:56:26', '2019-02-18 14:56:26');
INSERT INTO `admin_operation_log` VALUES ('459', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:56:26', '2019-02-18 14:56:26');
INSERT INTO `admin_operation_log` VALUES ('460', '1', 'admin/appoint/12/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:56:28', '2019-02-18 14:56:28');
INSERT INTO `admin_operation_log` VALUES ('461', '1', 'admin/appoint/12', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,D,C\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"vid\":\"6\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:56:38', '2019-02-18 14:56:38');
INSERT INTO `admin_operation_log` VALUES ('462', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:56:38', '2019-02-18 14:56:38');
INSERT INTO `admin_operation_log` VALUES ('463', '1', 'admin/appoint/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:56:44', '2019-02-18 14:56:44');
INSERT INTO `admin_operation_log` VALUES ('464', '1', 'admin/appoint/10', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,D,A\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"vid\":\"8\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:56:49', '2019-02-18 14:56:49');
INSERT INTO `admin_operation_log` VALUES ('465', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:56:49', '2019-02-18 14:56:49');
INSERT INTO `admin_operation_log` VALUES ('466', '1', 'admin/appoint/9/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:56:53', '2019-02-18 14:56:53');
INSERT INTO `admin_operation_log` VALUES ('467', '1', 'admin/appoint/9', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,C,C\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"vid\":\"6\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=2\"}', '2019-02-18 14:56:59', '2019-02-18 14:56:59');
INSERT INTO `admin_operation_log` VALUES ('468', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 14:56:59', '2019-02-18 14:56:59');
INSERT INTO `admin_operation_log` VALUES ('469', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\",\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:05', '2019-02-18 14:57:05');
INSERT INTO `admin_operation_log` VALUES ('470', '1', 'admin/appoint/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:08', '2019-02-18 14:57:08');
INSERT INTO `admin_operation_log` VALUES ('471', '1', 'admin/appoint/8', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,C,B\",\"cars\":\"\\u51ef\\u8fea\\u62c9\\u514bXT5\",\"vid\":\"8\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:14', '2019-02-18 14:57:14');
INSERT INTO `admin_operation_log` VALUES ('472', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:15', '2019-02-18 14:57:15');
INSERT INTO `admin_operation_log` VALUES ('473', '1', 'admin/appoint/7/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:17', '2019-02-18 14:57:17');
INSERT INTO `admin_operation_log` VALUES ('474', '1', 'admin/appoint/7', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,C,A\",\"cars\":\"\\u672c\\u7530crv\",\"vid\":\"3\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:20', '2019-02-18 14:57:20');
INSERT INTO `admin_operation_log` VALUES ('475', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:21', '2019-02-18 14:57:21');
INSERT INTO `admin_operation_log` VALUES ('476', '1', 'admin/appoint/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:23', '2019-02-18 14:57:23');
INSERT INTO `admin_operation_log` VALUES ('477', '1', 'admin/appoint/6', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,B,C\",\"cars\":\"\\u542f\\u8fb0T90\",\"vid\":\"14\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:29', '2019-02-18 14:57:29');
INSERT INTO `admin_operation_log` VALUES ('478', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:29', '2019-02-18 14:57:29');
INSERT INTO `admin_operation_log` VALUES ('479', '1', 'admin/appoint/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:32', '2019-02-18 14:57:32');
INSERT INTO `admin_operation_log` VALUES ('480', '1', 'admin/appoint/5', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,B,B\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"vid\":\"6\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:39', '2019-02-18 14:57:39');
INSERT INTO `admin_operation_log` VALUES ('481', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:39', '2019-02-18 14:57:39');
INSERT INTO `admin_operation_log` VALUES ('482', '1', 'admin/appoint/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:42', '2019-02-18 14:57:42');
INSERT INTO `admin_operation_log` VALUES ('483', '1', 'admin/appoint/4', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,B,A\",\"cars\":\"\\u5317\\u4eac\\u73b0\\u4ee3\\u83f2\\u65af\\u5854\",\"vid\":\"1\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:45', '2019-02-18 14:57:45');
INSERT INTO `admin_operation_log` VALUES ('484', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:45', '2019-02-18 14:57:45');
INSERT INTO `admin_operation_log` VALUES ('485', '1', 'admin/appoint/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:48', '2019-02-18 14:57:48');
INSERT INTO `admin_operation_log` VALUES ('486', '1', 'admin/appoint/3', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,A,C\",\"cars\":\"\\u542f\\u8fb0T90\",\"vid\":\"14\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:53', '2019-02-18 14:57:53');
INSERT INTO `admin_operation_log` VALUES ('487', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:53', '2019-02-18 14:57:53');
INSERT INTO `admin_operation_log` VALUES ('488', '1', 'admin/appoint/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:57:55', '2019-02-18 14:57:55');
INSERT INTO `admin_operation_log` VALUES ('489', '1', 'admin/appoint/2', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,A,B\",\"cars\":\"\\u672c\\u7530crv\",\"vid\":\"3\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:57:58', '2019-02-18 14:57:58');
INSERT INTO `admin_operation_log` VALUES ('490', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:57:59', '2019-02-18 14:57:59');
INSERT INTO `admin_operation_log` VALUES ('491', '1', 'admin/appoint/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:58:01', '2019-02-18 14:58:01');
INSERT INTO `admin_operation_log` VALUES ('492', '1', 'admin/appoint/1', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,A,A\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u6b27\\u84dd\\u5fb7\",\"vid\":\"6\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?page=3\"}', '2019-02-18 14:58:06', '2019-02-18 14:58:06');
INSERT INTO `admin_operation_log` VALUES ('493', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\"}', '2019-02-18 14:58:07', '2019-02-18 14:58:07');
INSERT INTO `admin_operation_log` VALUES ('494', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:58:24', '2019-02-18 14:58:24');
INSERT INTO `admin_operation_log` VALUES ('495', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12}]},{\\\"id\\\":13,\\\"children\\\":[{\\\"id\\\":14},{\\\"id\\\":15},{\\\"id\\\":16},{\\\"id\\\":18},{\\\"id\\\":17}]}]\"}', '2019-02-18 14:58:40', '2019-02-18 14:58:40');
INSERT INTO `admin_operation_log` VALUES ('496', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:58:40', '2019-02-18 14:58:40');
INSERT INTO `admin_operation_log` VALUES ('497', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2019-02-18 14:58:43', '2019-02-18 14:58:43');
INSERT INTO `admin_operation_log` VALUES ('498', '1', 'admin/vehicle', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:58:46', '2019-02-18 14:58:46');
INSERT INTO `admin_operation_log` VALUES ('499', '1', 'admin/record', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:58:54', '2019-02-18 14:58:54');
INSERT INTO `admin_operation_log` VALUES ('500', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:59:01', '2019-02-18 14:59:01');
INSERT INTO `admin_operation_log` VALUES ('501', '1', 'admin/record', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:59:04', '2019-02-18 14:59:04');
INSERT INTO `admin_operation_log` VALUES ('502', '1', 'admin/vehicle', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:59:15', '2019-02-18 14:59:15');
INSERT INTO `admin_operation_log` VALUES ('503', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 14:59:21', '2019-02-18 14:59:21');
INSERT INTO `admin_operation_log` VALUES ('504', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2019-02-18 14:59:40', '2019-02-18 14:59:40');
INSERT INTO `admin_operation_log` VALUES ('505', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"3\"}', '2019-02-18 14:59:44', '2019-02-18 14:59:44');
INSERT INTO `admin_operation_log` VALUES ('506', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2019-02-18 14:59:46', '2019-02-18 14:59:46');
INSERT INTO `admin_operation_log` VALUES ('507', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"15\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:00:20', '2019-02-18 15:00:20');
INSERT INTO `admin_operation_log` VALUES ('508', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:01:08', '2019-02-18 15:01:08');
INSERT INTO `admin_operation_log` VALUES ('509', '1', 'admin/answer/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:01:10', '2019-02-18 15:01:10');
INSERT INTO `admin_operation_log` VALUES ('510', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:01:18', '2019-02-18 15:01:18');
INSERT INTO `admin_operation_log` VALUES ('511', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:02:14', '2019-02-18 15:02:14');
INSERT INTO `admin_operation_log` VALUES ('512', '1', 'admin/vehicle', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:02', '2019-02-18 15:04:02');
INSERT INTO `admin_operation_log` VALUES ('513', '1', 'admin/vehicle/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:11', '2019-02-18 15:04:11');
INSERT INTO `admin_operation_log` VALUES ('514', '1', 'admin/vehicle', 'POST', '127.0.0.1', '{\"name\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/vehicle\"}', '2019-02-18 15:04:18', '2019-02-18 15:04:18');
INSERT INTO `admin_operation_log` VALUES ('515', '1', 'admin/vehicle', 'GET', '127.0.0.1', '[]', '2019-02-18 15:04:19', '2019-02-18 15:04:19');
INSERT INTO `admin_operation_log` VALUES ('516', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:21', '2019-02-18 15:04:21');
INSERT INTO `admin_operation_log` VALUES ('517', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2019-02-18 15:04:23', '2019-02-18 15:04:23');
INSERT INTO `admin_operation_log` VALUES ('518', '1', 'admin/appoint/21/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:26', '2019-02-18 15:04:26');
INSERT INTO `admin_operation_log` VALUES ('519', '1', 'admin/appoint/21', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,C,C\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"vid\":\"18\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&page=2\"}', '2019-02-18 15:04:32', '2019-02-18 15:04:32');
INSERT INTO `admin_operation_log` VALUES ('520', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 15:04:32', '2019-02-18 15:04:32');
INSERT INTO `admin_operation_log` VALUES ('521', '1', 'admin/appoint/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:36', '2019-02-18 15:04:36');
INSERT INTO `admin_operation_log` VALUES ('522', '1', 'admin/appoint/16', 'PUT', '127.0.0.1', '{\"specify\":\"A,B,B,A\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"vid\":\"18\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&page=2\"}', '2019-02-18 15:04:41', '2019-02-18 15:04:41');
INSERT INTO `admin_operation_log` VALUES ('523', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 15:04:42', '2019-02-18 15:04:42');
INSERT INTO `admin_operation_log` VALUES ('524', '1', 'admin/appoint/11/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:45', '2019-02-18 15:04:45');
INSERT INTO `admin_operation_log` VALUES ('525', '1', 'admin/appoint/11', 'PUT', '127.0.0.1', '{\"specify\":\"A,A,D,B\",\"cars\":\"\\u65e5\\u4ea7\\u65b0\\u5947\\u9a8f\",\"vid\":\"18\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&page=2\"}', '2019-02-18 15:04:50', '2019-02-18 15:04:50');
INSERT INTO `admin_operation_log` VALUES ('526', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"2\"}', '2019-02-18 15:04:50', '2019-02-18 15:04:50');
INSERT INTO `admin_operation_log` VALUES ('527', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"page\":\"3\",\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:04:52', '2019-02-18 15:04:52');
INSERT INTO `admin_operation_log` VALUES ('528', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2019-02-18 15:04:54', '2019-02-18 15:04:54');
INSERT INTO `admin_operation_log` VALUES ('529', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2019-02-18 15:04:56', '2019-02-18 15:04:56');
INSERT INTO `admin_operation_log` VALUES ('530', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"3\"}', '2019-02-18 15:04:59', '2019-02-18 15:04:59');
INSERT INTO `admin_operation_log` VALUES ('531', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2019-02-18 15:05:01', '2019-02-18 15:05:01');
INSERT INTO `admin_operation_log` VALUES ('532', '1', 'admin/vehicle', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:07:20', '2019-02-18 15:07:20');
INSERT INTO `admin_operation_log` VALUES ('533', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:07:25', '2019-02-18 15:07:25');
INSERT INTO `admin_operation_log` VALUES ('534', '1', 'admin/appoint/48', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:07:29', '2019-02-18 15:07:29');
INSERT INTO `admin_operation_log` VALUES ('535', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:07:39', '2019-02-18 15:07:39');
INSERT INTO `admin_operation_log` VALUES ('536', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"1\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:09:52', '2019-02-18 15:09:52');
INSERT INTO `admin_operation_log` VALUES ('537', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"2\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:09:55', '2019-02-18 15:09:55');
INSERT INTO `admin_operation_log` VALUES ('538', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"3\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:09:57', '2019-02-18 15:09:57');
INSERT INTO `admin_operation_log` VALUES ('539', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"4\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:00', '2019-02-18 15:10:00');
INSERT INTO `admin_operation_log` VALUES ('540', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"5\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:03', '2019-02-18 15:10:03');
INSERT INTO `admin_operation_log` VALUES ('541', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"6\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:41', '2019-02-18 15:10:41');
INSERT INTO `admin_operation_log` VALUES ('542', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"7\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:45', '2019-02-18 15:10:45');
INSERT INTO `admin_operation_log` VALUES ('543', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"8\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:50', '2019-02-18 15:10:50');
INSERT INTO `admin_operation_log` VALUES ('544', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"9\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:56', '2019-02-18 15:10:56');
INSERT INTO `admin_operation_log` VALUES ('545', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"10\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:10:58', '2019-02-18 15:10:58');
INSERT INTO `admin_operation_log` VALUES ('546', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"11\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:01', '2019-02-18 15:11:01');
INSERT INTO `admin_operation_log` VALUES ('547', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"12\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:03', '2019-02-18 15:11:03');
INSERT INTO `admin_operation_log` VALUES ('548', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"13\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:05', '2019-02-18 15:11:05');
INSERT INTO `admin_operation_log` VALUES ('549', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"14\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:23', '2019-02-18 15:11:23');
INSERT INTO `admin_operation_log` VALUES ('550', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"15\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:26', '2019-02-18 15:11:26');
INSERT INTO `admin_operation_log` VALUES ('551', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"16\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:29', '2019-02-18 15:11:29');
INSERT INTO `admin_operation_log` VALUES ('552', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"17\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:32', '2019-02-18 15:11:32');
INSERT INTO `admin_operation_log` VALUES ('553', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"18\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:11:34', '2019-02-18 15:11:34');
INSERT INTO `admin_operation_log` VALUES ('554', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:19:11', '2019-02-18 15:19:11');
INSERT INTO `admin_operation_log` VALUES ('555', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:19:14', '2019-02-18 15:19:14');
INSERT INTO `admin_operation_log` VALUES ('556', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:19:16', '2019-02-18 15:19:16');
INSERT INTO `admin_operation_log` VALUES ('557', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:19:17', '2019-02-18 15:19:17');
INSERT INTO `admin_operation_log` VALUES ('558', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:22:27', '2019-02-18 15:22:27');
INSERT INTO `admin_operation_log` VALUES ('559', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"A,B,B\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:22:49', '2019-02-18 15:22:49');
INSERT INTO `admin_operation_log` VALUES ('560', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"A,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:23:14', '2019-02-18 15:23:14');
INSERT INTO `admin_operation_log` VALUES ('561', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:23:52', '2019-02-18 15:23:52');
INSERT INTO `admin_operation_log` VALUES ('562', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,B,B\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:24:03', '2019-02-18 15:24:03');
INSERT INTO `admin_operation_log` VALUES ('563', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:24:09', '2019-02-18 15:24:09');
INSERT INTO `admin_operation_log` VALUES ('564', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"4\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:24:20', '2019-02-18 15:24:20');
INSERT INTO `admin_operation_log` VALUES ('565', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,B,B\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:24:53', '2019-02-18 15:24:53');
INSERT INTO `admin_operation_log` VALUES ('566', '1', 'admin/appoint/43/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:25:01', '2019-02-18 15:25:01');
INSERT INTO `admin_operation_log` VALUES ('567', '1', 'admin/appoint/43', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,B,B\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u52b2\\u70ab\",\"vid\":\"5\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&id=&specify=B%2CA%2CB%2CB&cars=&vid=&created_at%5Bstart%5D=&created_at%5Bend%5D=&updated_at%5Bstart%5D=&updated_at%5Bend%5D=\"}', '2019-02-18 15:25:10', '2019-02-18 15:25:10');
INSERT INTO `admin_operation_log` VALUES ('568', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,A,B,B\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:25:10', '2019-02-18 15:25:10');
INSERT INTO `admin_operation_log` VALUES ('569', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,A,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:25:14', '2019-02-18 15:25:14');
INSERT INTO `admin_operation_log` VALUES ('570', '1', 'admin/appoint/27/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:25:19', '2019-02-18 15:25:19');
INSERT INTO `admin_operation_log` VALUES ('571', '1', 'admin/appoint/27', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,B,A\",\"cars\":\"\\u5e7f\\u6c7d\\u4e09\\u83f1\\u52b2\\u70ab\",\"vid\":\"5\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&id=&specify=B%2CA%2CB%2CA&cars=&vid=&created_at%5Bstart%5D=&created_at%5Bend%5D=&updated_at%5Bstart%5D=&updated_at%5Bend%5D=\"}', '2019-02-18 15:25:27', '2019-02-18 15:25:27');
INSERT INTO `admin_operation_log` VALUES ('572', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,A,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:25:27', '2019-02-18 15:25:27');
INSERT INTO `admin_operation_log` VALUES ('573', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"13\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:26:52', '2019-02-18 15:26:52');
INSERT INTO `admin_operation_log` VALUES ('574', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":\"\\u542f\\u8fb0\",\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:27:18', '2019-02-18 15:27:18');
INSERT INTO `admin_operation_log` VALUES ('575', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:27:29', '2019-02-18 15:27:29');
INSERT INTO `admin_operation_log` VALUES ('576', '1', 'admin/answer', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:29:40', '2019-02-18 15:29:40');
INSERT INTO `admin_operation_log` VALUES ('577', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:31:52', '2019-02-18 15:31:52');
INSERT INTO `admin_operation_log` VALUES ('578', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"14\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:31:59', '2019-02-18 15:31:59');
INSERT INTO `admin_operation_log` VALUES ('579', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"13\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:32:05', '2019-02-18 15:32:05');
INSERT INTO `admin_operation_log` VALUES ('580', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,A,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:32:31', '2019-02-18 15:32:31');
INSERT INTO `admin_operation_log` VALUES ('581', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,B,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:32:49', '2019-02-18 15:32:49');
INSERT INTO `admin_operation_log` VALUES ('582', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":\"\\u672c\\u7530\\u601d\\u57df\",\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:33:01', '2019-02-18 15:33:01');
INSERT INTO `admin_operation_log` VALUES ('583', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"1\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:33:08', '2019-02-18 15:33:08');
INSERT INTO `admin_operation_log` VALUES ('584', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"2\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:33:13', '2019-02-18 15:33:13');
INSERT INTO `admin_operation_log` VALUES ('585', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,B,AA\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:33:38', '2019-02-18 15:33:38');
INSERT INTO `admin_operation_log` VALUES ('586', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":\"B,A,A,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:33:45', '2019-02-18 15:33:45');
INSERT INTO `admin_operation_log` VALUES ('587', '1', 'admin/appoint/25/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:34:04', '2019-02-18 15:34:04');
INSERT INTO `admin_operation_log` VALUES ('588', '1', 'admin/appoint/25', 'PUT', '127.0.0.1', '{\"specify\":\"B,A,A,A\",\"cars\":\"\\u542f\\u8fb0T60\",\"vid\":\"13\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&id=&specify=B%2CA%2CA%2CA&cars=&vid=&created_at%5Bstart%5D=&created_at%5Bend%5D=&updated_at%5Bstart%5D=&updated_at%5Bend%5D=\"}', '2019-02-18 15:34:11', '2019-02-18 15:34:11');
INSERT INTO `admin_operation_log` VALUES ('589', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,A,A,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:34:11', '2019-02-18 15:34:11');
INSERT INTO `admin_operation_log` VALUES ('590', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,B,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:34:20', '2019-02-18 15:34:20');
INSERT INTO `admin_operation_log` VALUES ('591', '1', 'admin/appoint/32/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:34:25', '2019-02-18 15:34:25');
INSERT INTO `admin_operation_log` VALUES ('592', '1', 'admin/appoint/32', 'PUT', '127.0.0.1', '{\"specify\":\"B,B,B,A\",\"cars\":\"\\u542f\\u8fb0T60\",\"vid\":\"13\",\"_token\":\"zQQbnMHdFLEjCLjQq6O61GVVx0fDdFh5Yg9aZTgn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8098\\/admin\\/appoint?&id=&specify=B%2CB%2CB%2CA&cars=&vid=&created_at%5Bstart%5D=&created_at%5Bend%5D=&updated_at%5Bstart%5D=&updated_at%5Bend%5D=\"}', '2019-02-18 15:34:32', '2019-02-18 15:34:32');
INSERT INTO `admin_operation_log` VALUES ('593', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":\"B,B,B,A\",\"cars\":null,\"vid\":null,\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:34:32', '2019-02-18 15:34:32');
INSERT INTO `admin_operation_log` VALUES ('594', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"2\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null},\"_pjax\":\"#pjax-container\"}', '2019-02-18 15:34:44', '2019-02-18 15:34:44');
INSERT INTO `admin_operation_log` VALUES ('595', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"4\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 15:34:51', '2019-02-18 15:34:51');
INSERT INTO `admin_operation_log` VALUES ('596', '1', 'admin/appoint', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":null,\"specify\":null,\"cars\":null,\"vid\":\"10\",\"created_at\":{\"start\":null,\"end\":null},\"updated_at\":{\"start\":null,\"end\":null}}', '2019-02-18 16:10:02', '2019-02-18 16:10:02');

-- ----------------------------
-- Table structure for `admin_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_permissions
-- ----------------------------
INSERT INTO `admin_permissions` VALUES ('1', 'All permission', '*', '', '*', null, null);
INSERT INTO `admin_permissions` VALUES ('2', 'Dashboard', 'dashboard', 'GET', '/', null, null);
INSERT INTO `admin_permissions` VALUES ('3', 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', null, null);
INSERT INTO `admin_permissions` VALUES ('4', 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', null, null);
INSERT INTO `admin_permissions` VALUES ('5', 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', null, null);
INSERT INTO `admin_permissions` VALUES ('6', 'Admin helpers', 'ext.helpers', null, '/helpers/*', '2019-02-14 03:31:41', '2019-02-14 03:31:41');

-- ----------------------------
-- Table structure for `admin_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES ('1', '2', null, null);

-- ----------------------------
-- Table structure for `admin_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES ('1', '1', null, null);

-- ----------------------------
-- Table structure for `admin_role_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_users
-- ----------------------------
INSERT INTO `admin_role_users` VALUES ('1', '1', null, null);

-- ----------------------------
-- Table structure for `admin_roles`
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES ('1', 'Administrator', 'administrator', '2019-02-14 03:28:45', '2019-02-14 03:28:45');

-- ----------------------------
-- Table structure for `admin_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `admin_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES ('1', 'admin', '$2y$10$z39lRBszy6f641o1JqUAvuo1ZfjZ/FALo4KzYzay6HZQKc5yrc3CS', 'Administrator', null, null, '2019-02-14 03:28:45', '2019-02-14 03:28:45');

-- ----------------------------
-- Table structure for `answer`
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tid` mediumint(10) DEFAULT NULL COMMENT '所属题',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  `specify` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '下标(用于选择时)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES ('1', '1', '小哥哥', '0', 'A', '2019-02-14 15:54:12', '2019-02-14 15:54:12');
INSERT INTO `answer` VALUES ('2', '1', '小姐姐', '1', 'B', '2019-02-14 15:54:38', '2019-02-14 15:54:38');
INSERT INTO `answer` VALUES ('3', '2', '是', '0', 'A', '2019-02-14 15:55:01', '2019-02-14 15:55:01');
INSERT INTO `answer` VALUES ('4', '2', '否', '1', 'B', '2019-02-14 15:55:12', '2019-02-14 15:55:12');
INSERT INTO `answer` VALUES ('5', '3', '颜值突出', '0', 'A', '2019-02-14 15:56:09', '2019-02-14 15:56:09');
INSERT INTO `answer` VALUES ('6', '3', '节能环保', '1', 'B', '2019-02-14 15:56:24', '2019-02-14 15:56:24');
INSERT INTO `answer` VALUES ('7', '3', '空间大', '2', 'C', '2019-02-14 15:56:40', '2019-02-14 15:56:40');
INSERT INTO `answer` VALUES ('8', '3', '配置高', '3', 'D', '2019-02-14 15:56:56', '2019-02-14 15:56:56');
INSERT INTO `answer` VALUES ('9', '4', '三五好友品茶谈人生', '0', 'A', '2019-02-14 15:57:21', '2019-02-14 15:57:21');
INSERT INTO `answer` VALUES ('10', '4', '成群结队酒吧夜生活', '1', 'B', '2019-02-14 15:57:49', '2019-02-14 15:57:49');
INSERT INTO `answer` VALUES ('11', '4', '独自一人在家做肥宅', '2', 'C', '2019-02-14 15:58:08', '2019-02-14 15:58:08');

-- ----------------------------
-- Table structure for `appoint`
-- ----------------------------
DROP TABLE IF EXISTS `appoint`;
CREATE TABLE `appoint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `specify` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最终选择半角逗号分开如A,A,A,A',
  `cars` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '测试结果是什么车',
  `vid` mediumint(10) DEFAULT NULL COMMENT '所属车型',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of appoint
-- ----------------------------
INSERT INTO `appoint` VALUES ('1', 'A,A,A,A', '广汽三菱欧蓝德', '6', '2019-02-14 16:02:09', '2019-02-18 14:58:06');
INSERT INTO `appoint` VALUES ('2', 'A,A,A,B', '本田crv', '3', '2019-02-14 16:03:24', '2019-02-18 14:57:58');
INSERT INTO `appoint` VALUES ('3', 'A,A,A,C', '启辰T90', '14', '2019-02-15 08:39:41', '2019-02-18 14:57:53');
INSERT INTO `appoint` VALUES ('4', 'A,A,B,A', '北京现代菲斯塔', '1', '2019-02-15 08:40:03', '2019-02-18 14:57:45');
INSERT INTO `appoint` VALUES ('5', 'A,A,B,B', '广汽三菱欧蓝德', '6', '2019-02-15 08:40:35', '2019-02-18 14:57:39');
INSERT INTO `appoint` VALUES ('6', 'A,A,B,C', '启辰T90', '14', '2019-02-15 08:42:08', '2019-02-18 14:57:29');
INSERT INTO `appoint` VALUES ('7', 'A,A,C,A', '本田crv', '3', '2019-02-15 08:42:39', '2019-02-18 14:57:20');
INSERT INTO `appoint` VALUES ('8', 'A,A,C,B', '凯迪拉克XT5', '8', '2019-02-15 08:43:10', '2019-02-18 14:57:14');
INSERT INTO `appoint` VALUES ('9', 'A,A,C,C', '广汽三菱欧蓝德', '6', '2019-02-15 08:43:37', '2019-02-18 14:56:59');
INSERT INTO `appoint` VALUES ('10', 'A,A,D,A', '凯迪拉克XT5', '8', '2019-02-15 08:44:01', '2019-02-18 14:56:49');
INSERT INTO `appoint` VALUES ('11', 'A,A,D,B', '日产新奇骏', '18', '2019-02-15 08:44:22', '2019-02-18 15:04:50');
INSERT INTO `appoint` VALUES ('12', 'A,A,D,C', '广汽三菱欧蓝德', '6', '2019-02-15 08:44:42', '2019-02-18 14:56:38');
INSERT INTO `appoint` VALUES ('13', 'A,B,A,A', '路虎揽胜', '12', '2019-02-15 08:45:01', '2019-02-18 14:56:26');
INSERT INTO `appoint` VALUES ('14', 'A,B,A,B', '林肯领航员', '10', '2019-02-15 08:45:30', '2019-02-18 14:56:17');
INSERT INTO `appoint` VALUES ('15', 'A,B,A,C', '上汽大众途昂', '16', '2019-02-15 08:45:51', '2019-02-18 14:55:16');
INSERT INTO `appoint` VALUES ('16', 'A,B,B,A', '日产新奇骏', '18', '2019-02-15 08:46:11', '2019-02-18 15:04:41');
INSERT INTO `appoint` VALUES ('17', 'A,B,B,B', '凯迪拉克XT5', '8', '2019-02-15 08:46:31', '2019-02-18 14:55:01');
INSERT INTO `appoint` VALUES ('18', 'A,B,B,C', '上汽大众途昂', '16', '2019-02-15 08:46:54', '2019-02-18 14:54:52');
INSERT INTO `appoint` VALUES ('19', 'A,B,C,A', '林肯领航员', '10', '2019-02-15 08:47:21', '2019-02-18 14:54:44');
INSERT INTO `appoint` VALUES ('20', 'A,B,C,B', '路虎揽胜', '12', '2019-02-15 08:47:40', '2019-02-18 14:54:37');
INSERT INTO `appoint` VALUES ('21', 'A,B,C,C', '日产新奇骏', '18', '2019-02-15 08:48:03', '2019-02-18 15:04:32');
INSERT INTO `appoint` VALUES ('22', 'A,B,D,A', '路虎揽胜', '12', '2019-02-15 08:48:31', '2019-02-18 14:52:43');
INSERT INTO `appoint` VALUES ('23', 'A,B,D,B', '林肯领航员', '10', '2019-02-15 08:48:51', '2019-02-18 14:52:35');
INSERT INTO `appoint` VALUES ('24', 'A,B,D,C', '凯迪拉克XT5', '8', '2019-02-15 08:49:14', '2019-02-18 14:52:27');
INSERT INTO `appoint` VALUES ('25', 'B,A,A,A', '启辰T60', '13', '2019-02-15 08:49:46', '2019-02-18 15:34:11');
INSERT INTO `appoint` VALUES ('26', 'B,B,A,A', '路虎极光', '11', '2019-02-15 08:50:18', '2019-02-18 14:52:05');
INSERT INTO `appoint` VALUES ('27', 'B,A,B,A', '广汽三菱劲炫', '5', '2019-02-15 08:50:38', '2019-02-18 15:25:27');
INSERT INTO `appoint` VALUES ('28', 'B,A,C,A', '林肯MKC', '9', '2019-02-15 08:50:53', '2019-02-18 14:51:52');
INSERT INTO `appoint` VALUES ('29', 'B,A,D,A', '路虎极光', '11', '2019-02-15 08:51:12', '2019-02-18 14:51:42');
INSERT INTO `appoint` VALUES ('30', 'B,A,A,B', '路虎极光', '11', '2019-02-15 08:51:35', '2019-02-18 14:51:33');
INSERT INTO `appoint` VALUES ('31', 'B,A,A,C', '北京现代领动', '2', '2019-02-15 08:51:50', '2019-02-18 14:51:25');
INSERT INTO `appoint` VALUES ('32', 'B,B,B,A', '启辰T60', '13', '2019-02-15 08:52:07', '2019-02-18 15:34:32');
INSERT INTO `appoint` VALUES ('33', 'B,B,C,A', '林肯MKC', '9', '2019-02-15 08:52:23', '2019-02-18 14:51:06');
INSERT INTO `appoint` VALUES ('34', 'B,B,D,A', '路虎极光', '11', '2019-02-15 08:52:45', '2019-02-18 14:50:58');
INSERT INTO `appoint` VALUES ('35', 'B,B,A,B', '上汽大众途岳', '17', '2019-02-15 08:52:57', '2019-02-18 14:50:50');
INSERT INTO `appoint` VALUES ('36', 'B,B,A,C', '本田思域', '4', '2019-02-15 08:53:11', '2019-02-18 14:50:42');
INSERT INTO `appoint` VALUES ('37', 'B,B,D,C', '凯迪拉克XT4', '7', '2019-02-15 08:53:28', '2019-02-18 14:50:33');
INSERT INTO `appoint` VALUES ('38', 'B,B,C,C', '上汽大众途岳', '17', '2019-02-15 08:53:42', '2019-02-18 14:50:21');
INSERT INTO `appoint` VALUES ('39', 'B,B,B,B', '本田思域', '4', '2019-02-15 08:53:54', '2019-02-18 14:50:02');
INSERT INTO `appoint` VALUES ('40', 'B,B,B,C', '本田思域', '4', '2019-02-15 08:54:08', '2019-02-18 14:49:55');
INSERT INTO `appoint` VALUES ('41', 'B,B,C,B', '林肯MKC', '9', '2019-02-15 08:54:23', '2019-02-18 14:49:49');
INSERT INTO `appoint` VALUES ('42', 'B,B,D,B', '路虎极光', '11', '2019-02-15 08:54:54', '2019-02-18 14:49:43');
INSERT INTO `appoint` VALUES ('43', 'B,A,B,B', '广汽三菱劲炫', '5', '2019-02-15 08:55:13', '2019-02-18 15:25:10');
INSERT INTO `appoint` VALUES ('44', 'B,A,B,C', '日产骐达', '15', '2019-02-15 08:55:27', '2019-02-18 14:49:25');
INSERT INTO `appoint` VALUES ('45', 'B,A,C,B', '林肯MKC', '9', '2019-02-15 08:55:42', '2019-02-18 14:49:15');
INSERT INTO `appoint` VALUES ('46', 'B,A,C,C', '林肯MKC', '9', '2019-02-15 08:55:56', '2019-02-18 14:49:08');
INSERT INTO `appoint` VALUES ('47', 'B,A,D,B', '路虎极光', '11', '2019-02-15 08:56:12', '2019-02-18 14:48:56');
INSERT INTO `appoint` VALUES ('48', 'B,A,D,C', '凯迪拉克XT4', '7', '2019-02-15 08:56:35', '2019-02-18 14:48:44');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2016_01_04_173148_create_admin_tables', '1');
INSERT INTO `migrations` VALUES ('2', '2019_02_14_142839_create_topic_table', '2');
INSERT INTO `migrations` VALUES ('3', '2019_02_14_143320_create_answer_table', '3');
INSERT INTO `migrations` VALUES ('4', '2019_02_14_143653_create_appoint_table', '4');
INSERT INTO `migrations` VALUES ('5', '2019_02_14_143952_create_record_table', '5');
INSERT INTO `migrations` VALUES ('6', '2019_02_18_141729_create_vehicle_table', '6');

-- ----------------------------
-- Table structure for `record`
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specify` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of record
-- ----------------------------
INSERT INTO `record` VALUES ('1', '最终车型', 'A,A,A,A', '用户名', '2019-02-14 16:41:39', '2019-02-14 16:41:39');
INSERT INTO `record` VALUES ('2', '林肯领航员', 'A,B,A,B', '测试用户', '2019-02-18 16:09:07', '2019-02-18 16:09:07');
INSERT INTO `record` VALUES ('3', '林肯领航员', 'A,B,A,B', '测试用户', '2019-02-18 16:09:36', '2019-02-18 16:09:36');
INSERT INTO `record` VALUES ('4', '林肯领航员', 'A,B,A,B', '测试用户u', '2019-02-18 16:11:05', '2019-02-18 16:11:05');
INSERT INTO `record` VALUES ('5', '凯迪拉克XT5', 'A,B,B,B', '测试用户y', '2019-02-18 16:11:37', '2019-02-18 16:11:37');

-- ----------------------------
-- Table structure for `topic`
-- ----------------------------
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of topic
-- ----------------------------
INSERT INTO `topic` VALUES ('1', '性别？', '0');
INSERT INTO `topic` VALUES ('2', '是否单身？', '1');
INSERT INTO `topic` VALUES ('3', '选车时，您最在意的一点是', '2');
INSERT INTO `topic` VALUES ('4', '您喜欢的生活方式是？', '3');

-- ----------------------------
-- Table structure for `vehicle`
-- ----------------------------
DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '车型名称',
  `imgurl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '车型图片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of vehicle
-- ----------------------------
INSERT INTO `vehicle` VALUES ('1', '北京现代菲斯塔', 'images/a6d581a102de402db6b4e91320f7a11b.png');
INSERT INTO `vehicle` VALUES ('2', '北京现代领动', 'images/7c7308797c0ba0460c1932cfe5d0f591.png');
INSERT INTO `vehicle` VALUES ('3', '本田crv', 'images/8e8e53cc6a5c5ac24141045f77677f9b.png');
INSERT INTO `vehicle` VALUES ('4', '本田思域', 'images/13602b6855f2a123c41d74408fd441a7.png');
INSERT INTO `vehicle` VALUES ('5', '广汽三菱劲炫', 'images/184561e8523fa46183ff6213a831e5a3.png');
INSERT INTO `vehicle` VALUES ('6', '广汽三菱欧蓝德', 'images/f2f504ea69aa079cac74c1af1a8d44d1.png');
INSERT INTO `vehicle` VALUES ('7', '凯迪拉克XT4', 'images/9af20cfe9e4d49f807adfca8673e23c7.png');
INSERT INTO `vehicle` VALUES ('8', '凯迪拉克XT50', 'images/1fef8ea71f36cd1559c69ad7c53e268d.png');
INSERT INTO `vehicle` VALUES ('9', '林肯MKC', 'images/ba7cccc6a2939abff29d94f884b5aa12.png');
INSERT INTO `vehicle` VALUES ('10', '林肯领航员', 'images/f805e0980d136c14c2cc747bc47b9c40.png');
INSERT INTO `vehicle` VALUES ('11', '路虎极光', 'images/2543c417137d38059b4f93cfb096cae0.png');
INSERT INTO `vehicle` VALUES ('12', '路虎揽胜', 'images/0f4a9744f33255e77696bc68edd1bf37.png');
INSERT INTO `vehicle` VALUES ('13', '启辰T60', 'images/aeeb11bb47240b55a70eb19b6d9292bf.png');
INSERT INTO `vehicle` VALUES ('14', '启辰T90', 'images/c7925bc037a822f5e11dd1a8c9d341f9.png');
INSERT INTO `vehicle` VALUES ('15', '日产骐达', 'images/1871d82a6d39d372df72c03d3323ef54.png');
INSERT INTO `vehicle` VALUES ('16', '上汽大众途昂', 'images/2f95c82ac911d2944e694b1d4df16d08.png');
INSERT INTO `vehicle` VALUES ('17', '上汽大众途岳', 'images/e7311c36e4454fc8e39c1891ee712b84.png');
INSERT INTO `vehicle` VALUES ('18', '日产新奇骏', 'images/548dfece6d320822f030f989930244d8.png');
